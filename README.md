# demoNESse

_demoNESse_ is a [Nintendo Entertainment System (NES)](https://fr.wikipedia.org/wiki/Nintendo_Entertainment_System)
emulator written in [Rust](https://www.rust-lang.org/).

## Features

* CPU:
  * NTSC Model RP2A03: ✓
  * Known CPU quirks: ✓
  * Illegal Opcodes: ❌
* Interactive Debugger (CLI):
  * Examine CPU Registers: ✓
  * Examine CPU Status Register Flags: ✓
  * Examine CPU Stack: ❌
  * Add/List/Remove Breakpoints: ✓
  * Disassemble Memory: ✓
  * Dump Memory: ✓
  * Step: ✓
  * Step Over: ❌
  * Run: ✓


## Quickstart

Start _demoNESse_ and open interactive debugger shell with test data:

```shell
demoNESse
```
