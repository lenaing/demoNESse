use std::{
    io::{self, Write},
    sync::{
        atomic::{AtomicBool, Ordering},
        mpsc::{self, SyncSender, TryRecvError},
        Arc, RwLock,
    },
    thread,
    time::Duration,
};

use crate::demonesse::DemoNESse;

const SHELL_COMMANDS: &[(&str, &str); 12] = &[
    (
        "b | break | breakpoint [addr]",
        "List, add or remove breakpoints.",
    ),
    (
        "D | dump [count | start end]",
        "Dump requested portion of memory. Defaults to Zero Page.",
    ),
    (
        "d | dis | disassemble [count | start end]",
        "Disassemble requested portion of memory. Defaults to PC.",
    ),
    (
        "P | S | status [c | z | i | d | b | u | v | n] ",
        "Examine CPU Status register bits.",
    ),
    (
        "p | print [a | x | y | s | p | pc] ",
        "Examine CPU registers contents.",
    ),
    ("i | info", "Examine CPU state."),
    ("s | step", "Execute one step of emulation."),
    ("F | framestep", "Execute one step of emulation."),
    ("r | run", "Run emulation until next breakpoint."),
    ("R | resume", "Resume emulation."),
    ("h | help", "Print this help."),
    ("q | quit", "Quit the debugger."),
];

#[derive(Clone)]
pub struct CLIDebugger {
    demonesse: Arc<RwLock<DemoNESse>>,
    breakpoints: Arc<RwLock<Vec<u16>>>,
    debug_continuously: Arc<AtomicBool>,
    debug_until_end_of_frame: Arc<AtomicBool>,
    debug_thread: Option<Arc<RwLock<SyncSender<()>>>>,
}

impl CLIDebugger {
    pub fn new(demonesse: Arc<RwLock<DemoNESse>>) -> Self {
        demonesse.write().unwrap().debugging = true;
        let mut debugger = Self {
            demonesse,
            breakpoints: Arc::new(RwLock::new(Vec::new())),
            debug_continuously: Arc::new(AtomicBool::new(false)),
            debug_thread: None,
            debug_until_end_of_frame: Arc::new(AtomicBool::new(false)),
        };
        debugger.start_debug_thread();
        debugger
    }

    fn parse_number(value: &str) -> Result<u16, String> {
        match value.parse() {
            Ok(res) => Ok(res),
            Err(_) => {
                // Failed to parse as an integer, try again as hex string
                for prefix in vec!["$", "0x", "0X"] {
                    let value = value.trim_start_matches(prefix);
                    match u16::from_str_radix(value, 16) {
                        Ok(res) => return Ok(res),
                        Err(_) => (),
                    }
                }
                Err(format!("Failed to parse argument value '{}'.", value))
            }
        }
    }
}

impl CLIDebugger {
    fn print_banner(&self) {
        println!("😈 Welcome to the demoNESse debugger!");
    }

    fn print_help(&self) {
        self.print_banner();
        println!("");
        println!("Arguments can be decimal or hexadecimal values prefixed by '$' or '0x'.");
        println!("");
        println!("Available commands: ");
        println!("");
        for (cmd, description) in SHELL_COMMANDS {
            println!("{}", cmd);
            println!("\t{}", description);
        }
    }

    fn print_registers(&self) {
        let demonesse = self.demonesse.read().unwrap();
        let cpu = demonesse.cpu.read().unwrap();
        println!(
            "A: ${:02X?} X: ${:02X?} Y: ${:02X?} P: ${:02X?} SP: ${:02X?} PC: ${:04X?}",
            cpu.a(),
            cpu.x(),
            cpu.y(),
            cpu.p(),
            cpu.s(),
            cpu.pc(),
        );
    }

    fn print_status_register(&self) {
        let status = self.demonesse.read().unwrap().cpu.read().unwrap().p();
        let c = (status & (1 << 0) > 0) as u8;
        let z = (status & (1 << 1) > 0) as u8;
        let i = (status & (1 << 2) > 0) as u8;
        let d = (status & (1 << 3) > 0) as u8;
        let b = (status & (1 << 4) > 0) as u8;
        let u = (status & (1 << 5) > 0) as u8;
        let v = (status & (1 << 6) > 0) as u8;
        let n = (status & (1 << 7) > 0) as u8;
        println!(
            "N: {} V: {} U: {} B: {} D: {} I: {} Z: {} C: {}",
            n, v, u, b, d, i, z, c,
        );
    }

    fn print_breakpoints(&self) {
        let breakpoints = &self.breakpoints.read().unwrap();
        if breakpoints.len() > 0 {
            println!("Current breakpoints:");
            for i in breakpoints.iter() {
                println!("${:04X?}", i);
            }
        } else {
            println!("No breakpoints.")
        }
    }

    fn print_dump(&self, start: u16, end: u16, count: u16) {
        let demonesse = self.demonesse.read().unwrap();
        let bus = demonesse.bus.read().unwrap();
        let mut address = start;
        let mut read = 0;
        while (end > 0 && address < end) || (count > 0 && read < count) {
            if read % 0x10 == 0 {
                if address != start {
                    println!();
                }
                print!("${:04X?}: ", address);
            }
            print!("{:02X?} ", bus.cpu_read(address));
            address += 1;
            read += 1;
        }
        println!();
    }

    fn step(&mut self, single_step: bool) -> bool {
        let mut demonesse = self.demonesse.write().unwrap();

        // Let core know that we are debugging
        demonesse.debugging = true;

        // Stop continuous debug
        if single_step {
            self.debug_continuously.store(false, Ordering::SeqCst);
        }

        // Print current instruction disassembly
        {
            let cpu = demonesse.cpu.read().unwrap();
            for (_, line) in cpu.disassemble(cpu.pc(), 0, 1) {
                println!("{}", line);
            }
        }

        let mut had_frame_complete = false;
        let mut has_executed_instruction = false;
        loop {
            // Run one NES clock
            let (cpu_clock, frame_complete) = demonesse.clock();

            let cpu = demonesse.cpu.read().unwrap();
            let pc = cpu.pc();

            // Check for breakpoints
            if self.breakpoints.read().unwrap().contains(&pc) {
                println!("Breakpoint reached! (PC: ${:04X?})", pc);
                self.debug_continuously.store(false, Ordering::SeqCst);
                break;
            }

            if frame_complete {
                had_frame_complete = true;
            }

            if !has_executed_instruction {
                // Run until we have executed an instruction
                if cpu_clock && cpu.complete() {
                    has_executed_instruction = true;
                }
            } else {
                // Quit once we switched to a new instruction
                if !cpu.complete() {
                    break;
                }
            }
        }
        had_frame_complete
    }

    fn run(&mut self, until_end_of_frame: bool) {
        self.debug_until_end_of_frame
            .store(until_end_of_frame, Ordering::SeqCst);
        // Start continuous debug
        self.debug_continuously.store(true, Ordering::SeqCst);
    }

    pub fn shell(&mut self) {
        self.print_help();

        let mut last_command = vec!["".to_string()];
        loop {
            print!("> ");
            io::stdout().flush().unwrap();

            let mut read_command = String::new();

            io::stdin()
                .read_line(&mut read_command)
                .expect("Failed to command line.");

            let command = read_command
                .trim()
                .split(" ")
                .map(|s| s.to_string())
                .collect::<Vec<String>>();

            // Run last command on empty input
            let command = match command[0].as_str() {
                "" => last_command,
                _ => command,
            };

            let unknown_command = match command[0].as_str() {
                "b" | "break" | "breakpoint" => {
                    match command.len() {
                        1 => {
                            self.print_breakpoints();
                        }
                        2 => {
                            let breakpoint = CLIDebugger::parse_number(&command[1]).unwrap();
                            let breakpoints = &mut self.breakpoints.write().unwrap();
                            if !breakpoints.contains(&breakpoint) {
                                breakpoints.push(breakpoint);
                            } else {
                                breakpoints.retain(|&x| x != breakpoint);
                            }
                        }
                        _ => (),
                    }
                    false
                }
                "D" | "dump" => {
                    if let Some((start, end, count)) = match command.len() {
                        1 => {
                            // Zero Page
                            Some((0, 0, 0x100))
                        }
                        2 => {
                            // From PC
                            let start = self.demonesse.read().unwrap().cpu.read().unwrap().pc();
                            let count = CLIDebugger::parse_number(&command[1]).unwrap();
                            Some((start, 0, count))
                        }
                        3 => {
                            // Range
                            let start = CLIDebugger::parse_number(&command[1]).unwrap();
                            let end = CLIDebugger::parse_number(&command[2]).unwrap();
                            Some((start, end, 0))
                        }
                        _ => None,
                    } {
                        self.print_dump(start, end, count);
                        false
                    } else {
                        true
                    }
                }
                "d" | "dis" | "disassemble" => {
                    let demonesse = self.demonesse.read().unwrap();
                    let cpu = demonesse.cpu.read().unwrap();
                    if let Some((start, end, count)) = match command.len() {
                        1 => {
                            // Current instruction
                            Some((cpu.pc(), 0, 1))
                        }
                        2 => {
                            // From PC
                            let count = CLIDebugger::parse_number(&command[1]).unwrap();
                            Some((cpu.pc(), 0, count))
                        }
                        3 => {
                            // Range
                            let start = CLIDebugger::parse_number(&command[1]).unwrap();
                            let end = CLIDebugger::parse_number(&command[2]).unwrap();
                            Some((start, end, 0))
                        }
                        _ => None,
                    } {
                        for (_, line) in cpu.disassemble(start, end, count) {
                            println!("{}", line);
                        }
                        false
                    } else {
                        true
                    }
                }
                "r" | "run" => {
                    self.run(false);
                    false
                }
                "R" | "resume" => {
                    self.debug_continuously.store(false, Ordering::SeqCst);
                    self.demonesse.write().unwrap().debugging = false;
                    false
                }
                "s" | "step" => {
                    self.step(true);
                    false
                }
                "F" | "framestep" => {
                    self.run(true);
                    false
                }
                "P" | "S" | "status" => {
                    let status = self.demonesse.read().unwrap().cpu.read().unwrap().p();

                    if let Some(arg) = command.get(1) {
                        if let Some(bit) = match arg.as_str() {
                            "c" | "C" => Some(0),
                            "z" | "Z" => Some(1),
                            "i" | "I" => Some(2),
                            "d" | "D" => Some(3),
                            "b" | "B" => Some(4),
                            "u" | "U" => Some(5),
                            "v" | "V" => Some(6),
                            "n" | "N" => Some(7),
                            _ => None,
                        } {
                            println!("{}", ((status & (1 << bit)) > 0) as u8);
                        }
                    } else {
                        self.print_status_register();
                    }
                    false
                }
                "p" | "print" => {
                    if let Some(arg) = command.get(1) {
                        let demonesse = self.demonesse.read().unwrap();
                        let cpu = demonesse.cpu.read().unwrap();
                        match arg.as_str() {
                            "a" | "A" => println!("${:02X?}", cpu.a()),
                            "x" | "X" => println!("${:02X?}", cpu.x()),
                            "y" | "Y" => println!("${:02X?}", cpu.y()),
                            "s" | "S" => println!("${:02X?}", cpu.s()),
                            "p" | "P" => println!("${:02X?}", cpu.p()),
                            "pc" | "PC" => println!("${:04X?}", cpu.pc()),
                            _ => (),
                        }
                    } else {
                        self.print_registers();
                    }
                    false
                }
                "i" | "info" => {
                    println!("CPU State:");
                    self.print_status_register();
                    self.print_registers();
                    println!();
                    println!("Breakpoints:");
                    self.print_breakpoints();
                    println!();
                    println!("Zero Page:");
                    self.print_dump(0, 0, 0x100);
                    println!();
                    println!("PC Page:");
                    let pc = self.demonesse.read().unwrap().cpu.read().unwrap().pc();
                    self.print_dump(pc & 0xFF00, 0, 0x100);
                    false
                }
                "q" | "quit" => {
                    break;
                }
                "h" | "help" | _ => true,
            };

            if unknown_command && command.len() > 0 && !command[0].is_empty() {
                println!("Unknown command: '{}'", command.join(" "));
            }
            last_command = command;
        }
        self.stop_debug_thread();

        // Stop demoNESse
        self.demonesse.write().unwrap().running = false;
    }

    fn start_debug_thread(&mut self) {
        if self.debug_thread.is_some() {
            return;
        }

        let (tx, rx) = mpsc::sync_channel(1);

        self.debug_thread = Some(Arc::new(RwLock::new(tx)));

        let mut clone = self.clone();
        thread::Builder::new()
            .name("Debug".to_string())
            .spawn(move || loop {
                match rx.try_recv() {
                    Ok(_) | Err(TryRecvError::Disconnected) => {
                        break;
                    }
                    _ => {}
                }

                let debug_continuously = clone.debug_continuously.load(Ordering::SeqCst);
                let debug_until_end_of_frame =
                    clone.debug_until_end_of_frame.load(Ordering::SeqCst);

                if debug_continuously {
                    let had_frame_complete = clone.step(false);
                    if had_frame_complete && debug_until_end_of_frame {
                        // We reached end of frame, stop debugging
                        clone.debug_continuously.store(false, Ordering::SeqCst);
                    }
                } else {
                    std::thread::sleep(Duration::from_nanos(1));
                }
            })
            .unwrap();
    }

    fn stop_debug_thread(&mut self) {
        if self.debug_thread.is_some() {
            self.debug_thread
                .as_ref()
                .unwrap()
                .write()
                .unwrap()
                .send(())
                .unwrap();
            self.debug_thread = None;
        }
    }
}
