use std::{
    fs::File,
    io::{Read, Seek, SeekFrom},
};

use super::mapper::{mapper_000::Mapper000, Mapper};

pub struct Cartridge {
    prg_memory: Vec<u8>,
    chr_memory: Vec<u8>,
    mapper_id: u8,
    prg_banks: usize,
    chr_banks: usize,
    mapper: Box<dyn Mapper>,
}

impl Cartridge {
    pub fn new(filename: &str) -> Self {
        // TODO : Improve cartridge loading
        let mut header: [u8; 16] = [0; 16];

        let mut f = File::open(filename).unwrap();
        f.read_exact(&mut header).unwrap();

        // TODO : Exit if header does not match iNES signature.
        let _name = &header[0..4];
        let prg_rom_chunks = header[4]; // Size of PRG ROM in 16KB units
        let chr_rom_chunks = header[5]; // Size of CHR ROM in 8KB units (0 means CHR RAM)
        let flags6 = header[6];
        let flags7 = header[7];
        let _flags8 = header[8];
        let _flags9 = header[9];
        let _flags10 = header[10];

        if flags6 & (1 << 2) > 0 {
            f.seek(SeekFrom::Current(512)).unwrap();
        }

        let mapper_id = (flags7 >> 4) << 4 | (flags6 >> 4);

        let mut data: Vec<u8> = Vec::new();
        f.read_to_end(&mut data).unwrap();

        let prg_banks = prg_rom_chunks as usize;
        let prg_banks_offset = prg_banks * 16384;
        let mut prg_memory = Vec::new();
        prg_memory.extend_from_slice(&data[..prg_banks_offset]);

        let chr_banks = chr_rom_chunks as usize;

        let mut chr_memory = Vec::new();
        if chr_banks > 0 {
            chr_memory
                .extend_from_slice(&data[prg_banks_offset..prg_banks_offset + (chr_banks * 8192)]);
        }

        let mapper = match mapper_id {
            0 => Box::new(Mapper000::new(prg_banks, chr_banks)),
            _ => unimplemented!(),
        };

        Self {
            mapper_id,
            prg_banks,
            chr_banks,
            prg_memory,
            chr_memory,
            mapper,
        }
    }

    pub fn cpu_read(&self, addr: u16) -> u8 {
        let mut mapped_addr = 0 as u16;
        if self.mapper.cpu_read(addr, &mut mapped_addr) {
            self.prg_memory[mapped_addr as usize]
        } else {
            panic!("Mapper did not handle CPU read at ${:04X?}", addr);
        }
    }

    pub fn cpu_write(&mut self, addr: u16, data: u8) {
        let mut mapped_addr = 0 as u16;
        if self.mapper.cpu_write(addr, &mut mapped_addr) {
            self.prg_memory[mapped_addr as usize] = data;
        } else {
            panic!("Mapper did not handle CPU write at ${:04X?}", addr);
        }
    }

    pub fn ppu_read(&self, addr: u16) -> u8 {
        let mut mapped_addr = 0 as u16;
        if self.mapper.ppu_read(addr, &mut mapped_addr) {
            self.chr_memory[mapped_addr as usize]
        } else {
            panic!("Mapper did not handle PPU read at ${:04X?}", addr);
        }
    }

    pub fn ppu_write(&mut self, addr: u16, data: u8) {
        let mut mapped_addr = 0 as u16;
        if self.mapper.ppu_write(addr, &mut mapped_addr) {
            self.chr_memory[mapped_addr as usize] = data;
        } else {
            panic!("Mapper did not handle PPU write at ${:04X?}", addr);
        }
    }
}
