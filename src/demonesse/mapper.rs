pub mod mapper_000;

pub trait Mapper: Send + Sync {
    fn cpu_read(&self, addr: u16, mapped_addr: &mut u16) -> bool;
    fn cpu_write(&self, addr: u16, mapped_addr: &mut u16) -> bool;
    fn ppu_read(&self, addr: u16, mapped_addr: &mut u16) -> bool;
    fn ppu_write(&self, addr: u16, mapped_addr: &mut u16) -> bool;
}
