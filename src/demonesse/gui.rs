use std::{
    sync::{Arc, RwLock},
    time::{Duration, Instant, SystemTime},
};

use super::{
    frontend::{self, Message},
    DemoNESse,
};

pub struct GUI {
    /// DemoNESse
    demonesse: Arc<RwLock<DemoNESse>>,
    /// Requested Frames Per Second rate
    fps: u32,
    /// GUI Frontend
    frontend: Box<dyn frontend::Frontend>,
}

impl GUI {
    pub fn new(
        demonesse: Arc<RwLock<DemoNESse>>,
        fps: u32,
        frontend: Box<dyn frontend::Frontend>,
    ) -> Self {
        Self {
            demonesse,
            fps,
            frontend,
        }
    }

    fn current_second(&self) -> u64 {
        SystemTime::now()
            .duration_since(SystemTime::UNIX_EPOCH)
            .unwrap()
            .as_secs()
    }

    /// Main loop
    pub fn run(&mut self) {
        let mut last_second = self.current_second();
        let mut frame_count = 0;

        'running: loop {
            let frame_start = Instant::now();

            // Handle input
            match self.frontend.handle_input() {
                Message::DebugToggle => {
                    let mut demonesse = self.demonesse.write().unwrap();
                    demonesse.debugging = !demonesse.debugging;
                }
                Message::DebugStep => {
                    // Only if we are debugging
                    if self.demonesse.read().unwrap().is_debugging() {
                        let mut has_executed_instruction = false;
                        let mut demonesse = self.demonesse.write().unwrap();
                        'debug_step: loop {
                            // Run one NES clock
                            let (cpu_clock, _) = demonesse.clock();

                            let cpu = demonesse.cpu.read().unwrap();

                            if !has_executed_instruction {
                                // Run until we have executed an instruction
                                if cpu_clock && cpu.complete() {
                                    has_executed_instruction = true;
                                }
                            } else {
                                // Quit once we switched to a new instruction
                                if !cpu.complete() {
                                    break 'debug_step;
                                }
                            }
                        }
                    }
                }
                Message::DebugFrame => {
                    // Only if we are debugging
                    if self.demonesse.read().unwrap().is_debugging() {
                        'debug_frame: loop {
                            let (_, frame_complete) = self.demonesse.write().unwrap().clock();
                            if frame_complete {
                                break 'debug_frame;
                            }
                        }
                    }
                }
                Message::Reset => {
                    self.demonesse.write().unwrap().reset();
                }
                Message::Quit => break 'running,
                _ => {}
            }

            if self.demonesse.read().unwrap().is_running() {
                // We are still running

                if !self.demonesse.read().unwrap().is_debugging() {
                    // We are not debugging, run as fast as possible

                    let mut frame_complete = false;
                    while !frame_complete {
                        (_, frame_complete) = self.demonesse.write().unwrap().clock();
                    }
                }
            } else {
                break 'running;
            }

            // Render
            self.frontend
                .render(&self.demonesse.read().unwrap().ppu.read().unwrap().screen);
            frame_count += 1;

            // Update frontend with FPS infos
            let current_second = self.current_second();
            if current_second != last_second {
                self.frontend.infos("Unknown", frame_count);
                frame_count = 0;
                last_second = current_second;
            }

            // If we are FPS capped we may have some time to sleep
            if self.fps > 0 {
                let ms_per_frame = Duration::new(0, 1000000000 as u32 / self.fps);
                let elapsed_time_since_frame_start = frame_start.elapsed();
                if elapsed_time_since_frame_start < ms_per_frame {
                    std::thread::sleep(ms_per_frame - elapsed_time_since_frame_start);
                }
            }
        }
    }
}
