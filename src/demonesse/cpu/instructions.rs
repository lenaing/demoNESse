use std::fmt;

#[derive(Clone, Copy)]
pub enum AddressingMode {
    ACC, // Accumulator
    IMM, // Immediate
    ABS, // Absolute
    ZP0, // Zero Page
    ZPX, // Indexed Zero Page
    ZPY, // Indexed Zero Page
    ABX, // Indexed Absolute
    ABY, // Indexed Absolute
    IMP, // Implied
    REL, // Relative
    IZX, // Indexed Indirect
    IZY, // Indirect Indexed
    IND, // Absolute Indirect
}

#[rustfmt::skip]
#[derive(Debug, Clone, Copy)]
pub enum Operation {
    ADC, AND, ASL,
    BCC, BCS, BEQ, BIT, BMI, BNE, BPL, BRK, BVC, BVS,
    CLC, CLD, CLI, CLV, CMP, CPX, CPY,
    DEC, DEX, DEY,
    EOR,
    INC, INX, INY,
    JMP, JSR,
    LDA, LDX, LDY, LSR,
    NOP,
    ORA,
    PHA, PHP, PLA, PLP,
    ROL, ROR, RTI, RTS,
    SBC, SEC, SED, SEI, STA, STX, STY,
    TAX, TAY, TSX, TXA, TXS, TYA,
    XXX,
}

impl fmt::Display for Operation {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:?}", self)
    }
}

#[derive(Clone, Copy)]
pub struct Instruction {
    op: Operation,
    am: AddressingMode,
    cycles: u8,
}

impl Instruction {
    pub fn op(&self) -> Operation {
        self.op
    }

    pub fn am(&self) -> AddressingMode {
        self.am
    }

    pub fn cycles(&self) -> u8 {
        self.cycles
    }
}

// No cycles indicated in MOS MCS6500 Datasheet for DEX, DEY, but in A.1 of MOS Microcomputers Hardware Manual
#[rustfmt::skip]
static INSTRUCTIONS: &'static[Instruction] = &[
    // 0x00
    Instruction{op: Operation::BRK, am: AddressingMode::IMM, cycles: 7}, // 0x00
    Instruction{op: Operation::ORA, am: AddressingMode::IZX, cycles: 6}, // 0x01
    Instruction{op: Operation::XXX, am: AddressingMode::IMP, cycles: 2}, // 0x02
    Instruction{op: Operation::XXX, am: AddressingMode::IMP, cycles: 8}, // 0x03
    Instruction{op: Operation::XXX, am: AddressingMode::IMP, cycles: 3}, // 0x04
    Instruction{op: Operation::ORA, am: AddressingMode::ZP0, cycles: 3}, // 0x05
    Instruction{op: Operation::ASL, am: AddressingMode::ZP0, cycles: 5}, // 0x06
    Instruction{op: Operation::XXX, am: AddressingMode::IMP, cycles: 2}, // 0x07
    Instruction{op: Operation::PHP, am: AddressingMode::IMP, cycles: 3}, // 0x08
    Instruction{op: Operation::ORA, am: AddressingMode::IMM, cycles: 2}, // 0x09
    Instruction{op: Operation::ASL, am: AddressingMode::ACC, cycles: 2}, // 0x0A
    Instruction{op: Operation::XXX, am: AddressingMode::IMP, cycles: 2}, // 0x0B
    Instruction{op: Operation::XXX, am: AddressingMode::IMP, cycles: 2}, // 0x0C
    Instruction{op: Operation::ORA, am: AddressingMode::ABS, cycles: 4}, // 0x0D
    Instruction{op: Operation::ASL, am: AddressingMode::ABS, cycles: 6}, // 0x0E
    Instruction{op: Operation::XXX, am: AddressingMode::IMP, cycles: 2}, // 0x0F
    Instruction{op: Operation::BPL, am: AddressingMode::REL, cycles: 2}, // 0x10
    Instruction{op: Operation::ORA, am: AddressingMode::IZY, cycles: 5}, // 0x11
    Instruction{op: Operation::XXX, am: AddressingMode::IMP, cycles: 2}, // 0x12
    Instruction{op: Operation::XXX, am: AddressingMode::IMP, cycles: 2}, // 0x13
    Instruction{op: Operation::XXX, am: AddressingMode::IMP, cycles: 2}, // 0x14
    Instruction{op: Operation::ORA, am: AddressingMode::ZPX, cycles: 4}, // 0x15
    Instruction{op: Operation::ASL, am: AddressingMode::ZPX, cycles: 6}, // 0x16
    Instruction{op: Operation::XXX, am: AddressingMode::IMP, cycles: 2}, // 0x17
    Instruction{op: Operation::CLC, am: AddressingMode::IMP, cycles: 2}, // 0x18
    Instruction{op: Operation::ORA, am: AddressingMode::ABY, cycles: 4}, // 0x19
    Instruction{op: Operation::XXX, am: AddressingMode::IMP, cycles: 2}, // 0x1A
    Instruction{op: Operation::XXX, am: AddressingMode::IMP, cycles: 2}, // 0x1B
    Instruction{op: Operation::XXX, am: AddressingMode::IMP, cycles: 2}, // 0x1C
    Instruction{op: Operation::ORA, am: AddressingMode::ABX, cycles: 4}, // 0x1D
    Instruction{op: Operation::ASL, am: AddressingMode::ABX, cycles: 7}, // 0x1E
    Instruction{op: Operation::XXX, am: AddressingMode::IMP, cycles: 2}, // 0x1F
    // 0x20
    Instruction{op: Operation::JSR, am: AddressingMode::ABS, cycles: 6}, // 0x20
    Instruction{op: Operation::AND, am: AddressingMode::IZX, cycles: 6}, // 0x21
    Instruction{op: Operation::XXX, am: AddressingMode::IMP, cycles: 2}, // 0x22
    Instruction{op: Operation::XXX, am: AddressingMode::IMP, cycles: 2}, // 0x23
    Instruction{op: Operation::BIT, am: AddressingMode::ZP0, cycles: 3}, // 0x24
    Instruction{op: Operation::AND, am: AddressingMode::ZP0, cycles: 3}, // 0x25
    Instruction{op: Operation::ROL, am: AddressingMode::ZP0, cycles: 5}, // 0x26
    Instruction{op: Operation::XXX, am: AddressingMode::IMP, cycles: 2}, // 0x27
    Instruction{op: Operation::PLP, am: AddressingMode::IMP, cycles: 4}, // 0x28
    Instruction{op: Operation::AND, am: AddressingMode::IMM, cycles: 2}, // 0x29
    Instruction{op: Operation::ROL, am: AddressingMode::ACC, cycles: 2}, // 0x2A
    Instruction{op: Operation::XXX, am: AddressingMode::IMP, cycles: 2}, // 0x2B
    Instruction{op: Operation::BIT, am: AddressingMode::ABS, cycles: 4}, // 0x2C
    Instruction{op: Operation::AND, am: AddressingMode::ABS, cycles: 4}, // 0x2D
    Instruction{op: Operation::ROL, am: AddressingMode::ABS, cycles: 6}, // 0x2E
    Instruction{op: Operation::XXX, am: AddressingMode::IMP, cycles: 2}, // 0x2F
    Instruction{op: Operation::BMI, am: AddressingMode::REL, cycles: 2}, // 0x30
    Instruction{op: Operation::AND, am: AddressingMode::ZPY, cycles: 5}, // 0x31
    Instruction{op: Operation::XXX, am: AddressingMode::IMP, cycles: 2}, // 0x32
    Instruction{op: Operation::XXX, am: AddressingMode::IMP, cycles: 2}, // 0x33
    Instruction{op: Operation::XXX, am: AddressingMode::IMP, cycles: 2}, // 0x34
    Instruction{op: Operation::AND, am: AddressingMode::ZPX, cycles: 4}, // 0x35
    Instruction{op: Operation::ROL, am: AddressingMode::ZPX, cycles: 6}, // 0x36
    Instruction{op: Operation::XXX, am: AddressingMode::IMP, cycles: 2}, // 0x37
    Instruction{op: Operation::SEC, am: AddressingMode::IMP, cycles: 2}, // 0x38
    Instruction{op: Operation::AND, am: AddressingMode::ABY, cycles: 4}, // 0x39
    Instruction{op: Operation::XXX, am: AddressingMode::IMP, cycles: 2}, // 0x3A
    Instruction{op: Operation::XXX, am: AddressingMode::IMP, cycles: 2}, // 0x3B
    Instruction{op: Operation::XXX, am: AddressingMode::IMP, cycles: 2}, // 0x3C
    Instruction{op: Operation::AND, am: AddressingMode::ABX, cycles: 4}, // 0x3D
    Instruction{op: Operation::ROL, am: AddressingMode::ABX, cycles: 7}, // 0x3E
    Instruction{op: Operation::XXX, am: AddressingMode::IMP, cycles: 2}, // 0x3F
    // 0x40
    Instruction{op: Operation::RTI, am: AddressingMode::IMP, cycles: 6}, // 0x40
    Instruction{op: Operation::EOR, am: AddressingMode::ZPX, cycles: 6}, // 0x41
    Instruction{op: Operation::XXX, am: AddressingMode::IMP, cycles: 2}, // 0x42
    Instruction{op: Operation::XXX, am: AddressingMode::IMP, cycles: 2}, // 0x43
    Instruction{op: Operation::XXX, am: AddressingMode::IMP, cycles: 2}, // 0x44
    Instruction{op: Operation::EOR, am: AddressingMode::ZP0, cycles: 3}, // 0x45
    Instruction{op: Operation::LSR, am: AddressingMode::ZP0, cycles: 5}, // 0x46
    Instruction{op: Operation::XXX, am: AddressingMode::IMP, cycles: 2}, // 0x47
    Instruction{op: Operation::PHA, am: AddressingMode::IMP, cycles: 3}, // 0x48
    Instruction{op: Operation::EOR, am: AddressingMode::IMM, cycles: 2}, // 0x49
    Instruction{op: Operation::LSR, am: AddressingMode::ACC, cycles: 2}, // 0x4A
    Instruction{op: Operation::XXX, am: AddressingMode::IMP, cycles: 2}, // 0x4B
    Instruction{op: Operation::JMP, am: AddressingMode::ABS, cycles: 3}, // 0x4C
    Instruction{op: Operation::EOR, am: AddressingMode::ABS, cycles: 4}, // 0x4D
    Instruction{op: Operation::LSR, am: AddressingMode::ABS, cycles: 6}, // 0x4E
    Instruction{op: Operation::XXX, am: AddressingMode::IMP, cycles: 2}, // 0x4F
    Instruction{op: Operation::BVC, am: AddressingMode::REL, cycles: 2}, // 0x50
    Instruction{op: Operation::EOR, am: AddressingMode::ZPY, cycles: 5}, // 0x51
    Instruction{op: Operation::XXX, am: AddressingMode::IMP, cycles: 2}, // 0x52
    Instruction{op: Operation::XXX, am: AddressingMode::IMP, cycles: 2}, // 0x53
    Instruction{op: Operation::XXX, am: AddressingMode::IMP, cycles: 2}, // 0x54
    Instruction{op: Operation::EOR, am: AddressingMode::ZPX, cycles: 4}, // 0x55
    Instruction{op: Operation::LSR, am: AddressingMode::ZPX, cycles: 6}, // 0x56
    Instruction{op: Operation::XXX, am: AddressingMode::IMP, cycles: 2}, // 0x57
    Instruction{op: Operation::CLI, am: AddressingMode::IMP, cycles: 2}, // 0x58
    Instruction{op: Operation::EOR, am: AddressingMode::ABY, cycles: 4}, // 0x59
    Instruction{op: Operation::XXX, am: AddressingMode::IMP, cycles: 2}, // 0x5A
    Instruction{op: Operation::XXX, am: AddressingMode::IMP, cycles: 2}, // 0x5B
    Instruction{op: Operation::XXX, am: AddressingMode::IMP, cycles: 2}, // 0x5C
    Instruction{op: Operation::EOR, am: AddressingMode::ABX, cycles: 4}, // 0x5D
    Instruction{op: Operation::LSR, am: AddressingMode::ABX, cycles: 7}, // 0x5E
    Instruction{op: Operation::XXX, am: AddressingMode::IMP, cycles: 2}, // 0x5F
    // 0x60
    Instruction{op: Operation::RTS, am: AddressingMode::IMP, cycles: 6}, // 0x60
    Instruction{op: Operation::ADC, am: AddressingMode::ZPX, cycles: 6}, // 0x61
    Instruction{op: Operation::XXX, am: AddressingMode::IMP, cycles: 2}, // 0x62
    Instruction{op: Operation::XXX, am: AddressingMode::IMP, cycles: 2}, // 0x63
    Instruction{op: Operation::XXX, am: AddressingMode::IMP, cycles: 2}, // 0x64
    Instruction{op: Operation::ADC, am: AddressingMode::ZP0, cycles: 3}, // 0x65
    Instruction{op: Operation::ROR, am: AddressingMode::ZP0, cycles: 5}, // 0x66
    Instruction{op: Operation::XXX, am: AddressingMode::IMP, cycles: 2}, // 0x67
    Instruction{op: Operation::PLA, am: AddressingMode::IMP, cycles: 4}, // 0x68
    Instruction{op: Operation::ADC, am: AddressingMode::IMM, cycles: 2}, // 0x69
    Instruction{op: Operation::ROR, am: AddressingMode::ACC, cycles: 2}, // 0x6A
    Instruction{op: Operation::XXX, am: AddressingMode::IMP, cycles: 2}, // 0x6B
    Instruction{op: Operation::JMP, am: AddressingMode::IND, cycles: 5}, // 0x6C
    Instruction{op: Operation::ADC, am: AddressingMode::ABS, cycles: 5}, // 0x6D
    Instruction{op: Operation::ROR, am: AddressingMode::ABS, cycles: 6}, // 0x6E
    Instruction{op: Operation::XXX, am: AddressingMode::IMP, cycles: 2}, // 0x6F
    Instruction{op: Operation::BVS, am: AddressingMode::REL, cycles: 2}, // 0x70
    Instruction{op: Operation::ADC, am: AddressingMode::ZPY, cycles: 5}, // 0x71
    Instruction{op: Operation::XXX, am: AddressingMode::IMP, cycles: 2}, // 0x72
    Instruction{op: Operation::XXX, am: AddressingMode::IMP, cycles: 2}, // 0x73
    Instruction{op: Operation::XXX, am: AddressingMode::IMP, cycles: 2}, // 0x74
    Instruction{op: Operation::ADC, am: AddressingMode::ZPX, cycles: 4}, // 0x75
    Instruction{op: Operation::ROR, am: AddressingMode::ZPX, cycles: 6}, // 0x76
    Instruction{op: Operation::XXX, am: AddressingMode::IMP, cycles: 2}, // 0x77
    Instruction{op: Operation::SEI, am: AddressingMode::IMP, cycles: 2}, // 0x78
    Instruction{op: Operation::ADC, am: AddressingMode::ABY, cycles: 4}, // 0x79
    Instruction{op: Operation::XXX, am: AddressingMode::IMP, cycles: 2}, // 0x7A
    Instruction{op: Operation::XXX, am: AddressingMode::IMP, cycles: 2}, // 0x7B
    Instruction{op: Operation::XXX, am: AddressingMode::IMP, cycles: 2}, // 0x7C
    Instruction{op: Operation::ADC, am: AddressingMode::ABX, cycles: 4}, // 0x7D
    Instruction{op: Operation::ROR, am: AddressingMode::ABX, cycles: 7}, // 0x7E
    Instruction{op: Operation::XXX, am: AddressingMode::IMP, cycles: 2}, // 0x7F
    // 0x80
    Instruction{op: Operation::XXX, am: AddressingMode::IMP, cycles: 2}, // 0x80
    Instruction{op: Operation::STA, am: AddressingMode::ZPX, cycles: 6}, // 0x81
    Instruction{op: Operation::XXX, am: AddressingMode::IMP, cycles: 2}, // 0x82
    Instruction{op: Operation::XXX, am: AddressingMode::IMP, cycles: 2}, // 0x83
    Instruction{op: Operation::STY, am: AddressingMode::ZP0, cycles: 3}, // 0x84
    Instruction{op: Operation::STA, am: AddressingMode::ZP0, cycles: 3}, // 0x85
    Instruction{op: Operation::STX, am: AddressingMode::ZP0, cycles: 3}, // 0x86
    Instruction{op: Operation::XXX, am: AddressingMode::IMP, cycles: 2}, // 0x87
    Instruction{op: Operation::DEY, am: AddressingMode::IMP, cycles: 2}, // 0x88
    Instruction{op: Operation::XXX, am: AddressingMode::IMP, cycles: 2}, // 0x89
    Instruction{op: Operation::TXA, am: AddressingMode::IMP, cycles: 2}, // 0x8A
    Instruction{op: Operation::XXX, am: AddressingMode::IMP, cycles: 2}, // 0x8B
    Instruction{op: Operation::STY, am: AddressingMode::ABS, cycles: 4}, // 0x8C
    Instruction{op: Operation::STA, am: AddressingMode::ABS, cycles: 4}, // 0x8D
    Instruction{op: Operation::STX, am: AddressingMode::ABS, cycles: 4}, // 0x8E
    Instruction{op: Operation::XXX, am: AddressingMode::IMP, cycles: 2}, // 0x8F
    Instruction{op: Operation::BCC, am: AddressingMode::REL, cycles: 2}, // 0x90
    Instruction{op: Operation::STA, am: AddressingMode::ZPY, cycles: 6}, // 0x91
    Instruction{op: Operation::XXX, am: AddressingMode::IMP, cycles: 2}, // 0x92
    Instruction{op: Operation::XXX, am: AddressingMode::IMP, cycles: 2}, // 0x93
    Instruction{op: Operation::STY, am: AddressingMode::ZPX, cycles: 4}, // 0x94
    Instruction{op: Operation::STA, am: AddressingMode::ZPX, cycles: 4}, // 0x95
    Instruction{op: Operation::STX, am: AddressingMode::ZPY, cycles: 4}, // 0x96
    Instruction{op: Operation::XXX, am: AddressingMode::IMP, cycles: 2}, // 0x97
    Instruction{op: Operation::TYA, am: AddressingMode::IMP, cycles: 2}, // 0x98
    Instruction{op: Operation::STA, am: AddressingMode::ABY, cycles: 5}, // 0x99
    Instruction{op: Operation::TXS, am: AddressingMode::IMP, cycles: 2}, // 0x9A
    Instruction{op: Operation::XXX, am: AddressingMode::IMP, cycles: 2}, // 0x9B
    Instruction{op: Operation::XXX, am: AddressingMode::IMP, cycles: 2}, // 0x9C
    Instruction{op: Operation::STA, am: AddressingMode::ABX, cycles: 5}, // 0x9D
    Instruction{op: Operation::XXX, am: AddressingMode::IMP, cycles: 2}, // 0x9E
    Instruction{op: Operation::XXX, am: AddressingMode::IMP, cycles: 2}, // 0x9F
    // 0xA0
    Instruction{op: Operation::LDY, am: AddressingMode::IMM, cycles: 2}, // 0xA0
    Instruction{op: Operation::LDA, am: AddressingMode::ZPX, cycles: 6}, // 0xA1
    Instruction{op: Operation::LDX, am: AddressingMode::IMM, cycles: 2}, // 0xA2
    Instruction{op: Operation::XXX, am: AddressingMode::IMP, cycles: 2}, // 0xA3
    Instruction{op: Operation::LDY, am: AddressingMode::ZP0, cycles: 3}, // 0xA4
    Instruction{op: Operation::LDA, am: AddressingMode::ZP0, cycles: 3}, // 0xA5
    Instruction{op: Operation::LDX, am: AddressingMode::ZP0, cycles: 3}, // 0xA6
    Instruction{op: Operation::XXX, am: AddressingMode::IMP, cycles: 2}, // 0xA7
    Instruction{op: Operation::TAY, am: AddressingMode::IMP, cycles: 2}, // 0xA8
    Instruction{op: Operation::LDA, am: AddressingMode::IMM, cycles: 2}, // 0xA9
    Instruction{op: Operation::TAX, am: AddressingMode::IMP, cycles: 2}, // 0xAA
    Instruction{op: Operation::XXX, am: AddressingMode::IMP, cycles: 2}, // 0xAB
    Instruction{op: Operation::LDY, am: AddressingMode::ABS, cycles: 4}, // 0xAC
    Instruction{op: Operation::LDA, am: AddressingMode::ABS, cycles: 4}, // 0xAD
    Instruction{op: Operation::LDX, am: AddressingMode::ABS, cycles: 4}, // 0xAE
    Instruction{op: Operation::XXX, am: AddressingMode::IMP, cycles: 2}, // 0xAF
    Instruction{op: Operation::BCS, am: AddressingMode::REL, cycles: 2}, // 0xB0
    Instruction{op: Operation::LDA, am: AddressingMode::ZPY, cycles: 5}, // 0xB1
    Instruction{op: Operation::XXX, am: AddressingMode::IMP, cycles: 2}, // 0xB2
    Instruction{op: Operation::XXX, am: AddressingMode::IMP, cycles: 2}, // 0xB3
    Instruction{op: Operation::LDY, am: AddressingMode::ZPX, cycles: 4}, // 0xB4
    Instruction{op: Operation::LDA, am: AddressingMode::ZPX, cycles: 4}, // 0xB5
    Instruction{op: Operation::LDX, am: AddressingMode::ZPY, cycles: 4}, // 0xB6
    Instruction{op: Operation::XXX, am: AddressingMode::IMP, cycles: 2}, // 0xB7
    Instruction{op: Operation::CLV, am: AddressingMode::IMP, cycles: 2}, // 0xB8
    Instruction{op: Operation::LDA, am: AddressingMode::ABY, cycles: 4}, // 0xB9
    Instruction{op: Operation::TSX, am: AddressingMode::IMP, cycles: 2}, // 0xBA
    Instruction{op: Operation::XXX, am: AddressingMode::IMP, cycles: 2}, // 0xBB
    Instruction{op: Operation::LDY, am: AddressingMode::ABX, cycles: 4}, // 0xBC
    Instruction{op: Operation::LDA, am: AddressingMode::ABX, cycles: 4}, // 0xBD
    Instruction{op: Operation::LDX, am: AddressingMode::ABY, cycles: 4}, // 0xBE
    Instruction{op: Operation::XXX, am: AddressingMode::IMP, cycles: 2}, // 0xBF
    // 0xC0
    Instruction{op: Operation::CPY, am: AddressingMode::IMM, cycles: 2}, // 0xC0
    Instruction{op: Operation::CMP, am: AddressingMode::ZPX, cycles: 6}, // 0xC1
    Instruction{op: Operation::XXX, am: AddressingMode::IMP, cycles: 2}, // 0xC2
    Instruction{op: Operation::XXX, am: AddressingMode::IMP, cycles: 2}, // 0xC3
    Instruction{op: Operation::CPY, am: AddressingMode::ZP0, cycles: 3}, // 0xC4
    Instruction{op: Operation::CMP, am: AddressingMode::ZP0, cycles: 3}, // 0xC5
    Instruction{op: Operation::DEC, am: AddressingMode::ZP0, cycles: 5}, // 0xC6
    Instruction{op: Operation::XXX, am: AddressingMode::IMP, cycles: 2}, // 0xC7
    Instruction{op: Operation::INY, am: AddressingMode::IMP, cycles: 2}, // 0xC8
    Instruction{op: Operation::CMP, am: AddressingMode::IMM, cycles: 2}, // 0xC9
    Instruction{op: Operation::DEX, am: AddressingMode::IMP, cycles: 2}, // 0xCA
    Instruction{op: Operation::XXX, am: AddressingMode::IMP, cycles: 2}, // 0xCB
    Instruction{op: Operation::CPY, am: AddressingMode::ABS, cycles: 4}, // 0xCC
    Instruction{op: Operation::CMP, am: AddressingMode::ABS, cycles: 4}, // 0xCD
    Instruction{op: Operation::DEC, am: AddressingMode::ABS, cycles: 6}, // 0xCE
    Instruction{op: Operation::XXX, am: AddressingMode::IMP, cycles: 2}, // 0xCF
    Instruction{op: Operation::BNE, am: AddressingMode::REL, cycles: 2}, // 0xD0
    Instruction{op: Operation::CMP, am: AddressingMode::ZPY, cycles: 5}, // 0xD1
    Instruction{op: Operation::XXX, am: AddressingMode::IMP, cycles: 2}, // 0xD2
    Instruction{op: Operation::XXX, am: AddressingMode::IMP, cycles: 2}, // 0xD3
    Instruction{op: Operation::XXX, am: AddressingMode::IMP, cycles: 2}, // 0xD4
    Instruction{op: Operation::CMP, am: AddressingMode::ZPX, cycles: 4}, // 0xD5
    Instruction{op: Operation::DEC, am: AddressingMode::ZPX, cycles: 6}, // 0xD6
    Instruction{op: Operation::XXX, am: AddressingMode::IMP, cycles: 2}, // 0xD7
    Instruction{op: Operation::CLD, am: AddressingMode::IMP, cycles: 2}, // 0xD8
    Instruction{op: Operation::CMP, am: AddressingMode::ABY, cycles: 4}, // 0xD9
    Instruction{op: Operation::XXX, am: AddressingMode::IMP, cycles: 2}, // 0xDA
    Instruction{op: Operation::XXX, am: AddressingMode::IMP, cycles: 2}, // 0xDB
    Instruction{op: Operation::XXX, am: AddressingMode::IMP, cycles: 2}, // 0xDC
    Instruction{op: Operation::CMP, am: AddressingMode::ABX, cycles: 4}, // 0xDD
    Instruction{op: Operation::DEC, am: AddressingMode::ABX, cycles: 7}, // 0xDE
    Instruction{op: Operation::XXX, am: AddressingMode::IMP, cycles: 2}, // 0xDF
    // 0xE0
    Instruction{op: Operation::CPX, am: AddressingMode::IMM, cycles: 2}, // 0xE0
    Instruction{op: Operation::SBC, am: AddressingMode::ZPX, cycles: 6}, // 0xE1
    Instruction{op: Operation::XXX, am: AddressingMode::IMP, cycles: 2}, // 0xE2
    Instruction{op: Operation::XXX, am: AddressingMode::IMP, cycles: 2}, // 0xE3
    Instruction{op: Operation::CPX, am: AddressingMode::ZP0, cycles: 3}, // 0xE4
    Instruction{op: Operation::SBC, am: AddressingMode::ZP0, cycles: 3}, // 0xE5
    Instruction{op: Operation::INC, am: AddressingMode::ZP0, cycles: 5}, // 0xE6
    Instruction{op: Operation::XXX, am: AddressingMode::IMP, cycles: 2}, // 0xE7
    Instruction{op: Operation::INX, am: AddressingMode::IMP, cycles: 2}, // 0xE8
    Instruction{op: Operation::SBC, am: AddressingMode::IMM, cycles: 2}, // 0xE9
    Instruction{op: Operation::NOP, am: AddressingMode::IMP, cycles: 2}, // 0xEA
    Instruction{op: Operation::XXX, am: AddressingMode::IMP, cycles: 2}, // 0xEB
    Instruction{op: Operation::CPX, am: AddressingMode::ABS, cycles: 4}, // 0xEC
    Instruction{op: Operation::SBC, am: AddressingMode::ABS, cycles: 4}, // 0xED
    Instruction{op: Operation::INC, am: AddressingMode::ABS, cycles: 6}, // 0xEE
    Instruction{op: Operation::XXX, am: AddressingMode::IMP, cycles: 2}, // 0xEF
    Instruction{op: Operation::BEQ, am: AddressingMode::REL, cycles: 2}, // 0xF0
    Instruction{op: Operation::SBC, am: AddressingMode::ZPY, cycles: 5}, // 0xF1
    Instruction{op: Operation::XXX, am: AddressingMode::IMP, cycles: 2}, // 0xF2
    Instruction{op: Operation::XXX, am: AddressingMode::IMP, cycles: 2}, // 0xF3
    Instruction{op: Operation::XXX, am: AddressingMode::IMP, cycles: 2}, // 0xF4
    Instruction{op: Operation::SBC, am: AddressingMode::ZPX, cycles: 4}, // 0xF5
    Instruction{op: Operation::INC, am: AddressingMode::ZPX, cycles: 6}, // 0xF6
    Instruction{op: Operation::XXX, am: AddressingMode::IMP, cycles: 2}, // 0xF7
    Instruction{op: Operation::SED, am: AddressingMode::IMP, cycles: 2}, // 0xF8
    Instruction{op: Operation::SBC, am: AddressingMode::ABY, cycles: 4}, // 0xF9
    Instruction{op: Operation::XXX, am: AddressingMode::IMP, cycles: 2}, // 0xFA
    Instruction{op: Operation::XXX, am: AddressingMode::IMP, cycles: 2}, // 0xFB
    Instruction{op: Operation::XXX, am: AddressingMode::IMP, cycles: 2}, // 0xFC
    Instruction{op: Operation::SBC, am: AddressingMode::ABX, cycles: 4}, // 0xFD
    Instruction{op: Operation::INC, am: AddressingMode::ABX, cycles: 7}, // 0xFE
    Instruction{op: Operation::XXX, am: AddressingMode::IMP, cycles: 2}, // 0xFF
];

pub fn get(opcode: u8) -> Instruction {
    INSTRUCTIONS[opcode as usize]
}
