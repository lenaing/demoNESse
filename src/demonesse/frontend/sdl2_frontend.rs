//! An SDL2 Frontend without OpenGL

use sdl2::{
    event::Event,
    keyboard::Keycode,
    pixels::{Color, PixelFormatEnum},
    render::{Canvas, Texture},
    video::Window,
};

use crate::demonesse::ppu::{ScreenData, NES_RESOLUTION_HEIGHT, NES_RESOLUTION_WIDTH};

use super::{Frontend, Message};

/// An SDL2 Frontend without OpenGL
pub struct SDL2Frontend {
    /// Texture to update with NES PPU video memory content
    texture: Texture,
    /// Canvas on which to draw the associated Texture
    canvas: Canvas<Window>,
    /// Pump of input events to handle
    event_pump: sdl2::EventPump,
}

impl SDL2Frontend {
    pub fn new(use_vsync: bool) -> Self {
        // Initialize SDL 2
        let sdl_context = sdl2::init().unwrap();

        // Initialize Video Subsystem
        let video_subsystem = sdl_context.video().unwrap();

        // Create main Window
        let window = video_subsystem
            .window(
                "demoNESse",
                NES_RESOLUTION_WIDTH as u32,
                NES_RESOLUTION_HEIGHT as u32,
            )
            .position_centered()
            .build()
            .unwrap();

        // Create canvas
        let mut canvas = if use_vsync {
            window.into_canvas().present_vsync().build().unwrap()
        } else {
            window.into_canvas().build().unwrap()
        };
        let texture_creator = canvas.texture_creator();

        let texture = texture_creator
            .create_texture_streaming(
                PixelFormatEnum::RGB888,
                NES_RESOLUTION_WIDTH as u32,
                NES_RESOLUTION_HEIGHT as u32,
            )
            .unwrap();
        canvas.set_draw_color(Color::RGB(0, 0, 0));
        canvas.clear();
        canvas.present();

        // Grab event pump
        let event_pump = sdl_context.event_pump().unwrap();

        println!("SDL2 Frontend running on {}", sdl2::get_platform());
        println!(
            "SDL2 Version: {} - {} ({})",
            sdl2::version::version(),
            sdl2::version::revision_number(),
            sdl2::version::revision()
        );

        Self {
            texture,
            canvas,
            event_pump,
        }
    }
}

impl Frontend for SDL2Frontend {
    fn handle_input(&mut self) -> Message {
        let mut message = Message::None;

        for event in self.event_pump.poll_iter() {
            match event {
                Event::Quit { .. }
                | Event::KeyDown {
                    keycode: Some(Keycode::Escape),
                    ..
                } => {
                    message = Message::Quit;
                }

                Event::KeyDown {
                    keycode: Some(Keycode::Space),
                    ..
                } => {
                    message = Message::DebugToggle;
                }

                Event::KeyDown {
                    keycode: Some(Keycode::C),
                    ..
                } => {
                    message = Message::DebugStep;
                }

                Event::KeyDown {
                    keycode: Some(Keycode::F),
                    ..
                } => {
                    message = Message::DebugFrame;
                }

                Event::KeyDown {
                    keycode: Some(Keycode::R),
                    ..
                } => {
                    message = Message::Reset;
                }

                _ => {}
            }
        }
        message
    }

    fn render(&mut self, screen: &ScreenData) {
        self.canvas.set_draw_color(Color::RGB(0, 0, 0));
        self.canvas.clear();
        self.texture
            .update(None, screen, NES_RESOLUTION_WIDTH * 4)
            .unwrap();
        self.canvas.copy(&self.texture, None, None).unwrap();
        self.canvas.present();
    }

    fn infos(&mut self, rom_title: &str, fps: u32) {
        let demoNESse = "demoNESse";
        let title = if rom_title.is_empty() {
            demoNESse.to_string()
        } else {
            format!("{} - {} - {} FPS", demoNESse, rom_title, fps)
        };
        self.canvas.window_mut().set_title(&title).unwrap();
    }
}
