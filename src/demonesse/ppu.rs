//! NES PPU, derivative of the Texas Instrument TMS9918.
//!
//! Made by Ricoh.
//!
//! Currently implements the R2C02 Chip only, used in NTSC NES.
use rand::prelude::*;
use std::sync::{Arc, RwLock};

pub const NES_RESOLUTION_WIDTH: usize = 256;
pub const NES_RESOLUTION_HEIGHT: usize = 240;
const NES_INTERNAL_VRAM: usize = 2 * 1024;
const NES_PALETTE_COLORS: usize = 64;
const NES_OAM: usize = 256;

use super::cartridge::Cartridge;

pub type ScreenData = [u8; NES_RESOLUTION_HEIGHT * NES_RESOLUTION_WIDTH * 4];

/// NES PPU.
///
/// # Nesdev Wiki
///
/// <https://www.nesdev.org/wiki/PPU>
/// <https://www.nesdev.org/wiki/PPU_memory_map>
///
/// The PPU addresses a 16kB space.
///
/// It can be used to access 12 kB of memory:
/// * 8 kB of pattern tables (CHR ROM or RAM on the cartridge)
/// * 4 kB of name tables
///   * The PPU has internal vRAM (2 kB)
///   * Can also be remapped partly or fully to cartridge RAM
///
/// Palettes controls which colors are associated to various indices.
///
/// |Address Range|Size  |Description            |
/// |-------------|------|-----------------------|
/// |$0000-$0FFF  |$1000 |Pattern table 0        |
/// |$1000-$1FFF  |$1000 |Pattern table 1        |
/// |$2000-$23FF  |$0400 |Nametable 0            |
/// |$2400-$27FF  |$0400 |Nametable 1            |
/// |$2800-$2BFF  |$0400 |Nametable 2            |
/// |$2C00-$2FFF  |$0400 |Nametable 3            |
/// |$3000-$3EFF  |$0F00 |Mirrors of $2000-$2EFF |
/// |$3F00-$3F1F  |$0020 |Palette RAM indexes    |
/// |$3F20-$3FFF  |$00E0 |Mirrors of $3F00-$3F1F |
///
/// The PPU contains also 256 bytes of OAM (Object Attribute Memory) to stores the position,
/// orientation, shape and colors of sprites (independent moving objects).
pub struct PPU {
    /// 2 kB internal VRAM for Name Tables
    _vram: [u8; NES_INTERNAL_VRAM],
    /// 64 colors palette
    palette: [(u8, u8, u8); NES_PALETTE_COLORS],
    /// 256 bytes OAM
    _oam: [u8; NES_OAM],
    cartridge: Option<Arc<RwLock<Cartridge>>>,
    scanline: i16,
    cycle: i16,
    pub screen: ScreenData,
}

impl PPU {
    pub fn new() -> Self {
        Self {
            _vram: [0; NES_INTERNAL_VRAM],
            palette: PPU::palette_2C02(),
            _oam: [0; NES_OAM],
            scanline: 0,
            cycle: 0,
            cartridge: None,
            screen: [0; NES_RESOLUTION_HEIGHT * NES_RESOLUTION_WIDTH * 4],
        }
    }

    /// 2C02 palette.
    ///
    /// # Nesdev Wiki
    ///
    /// <https://www.nesdev.org/wiki/PPU_palettes#2C02>
    fn palette_2C02() -> [(u8, u8, u8); NES_PALETTE_COLORS] {
        let mut palette = [(0, 0, 0); NES_PALETTE_COLORS];
        palette[0x00] = (84, 84, 84); // Dark Gray
        palette[0x01] = (0, 30, 116); // Dark Azure
        palette[0x02] = (8, 16, 144); // Dark Blue [Navy]
        palette[0x03] = (48, 0, 136); // Dark Violet
        palette[0x04] = (68, 0, 100); // Dark Magenta
        palette[0x05] = (92, 0, 48); // Dark Rose
        palette[0x06] = (84, 4, 0); // Dark Red/Maroon [Maroon]
        palette[0x07] = (60, 24, 0); // Dark Orange
        palette[0x08] = (32, 42, 0); // Dark Yellow/Olive
        palette[0x09] = (8, 58, 0); // Dark Chartreuse
        palette[0x0A] = (0, 64, 0); // Dark Green
        palette[0x0B] = (0, 60, 0); // Dark Spring
        palette[0x0C] = (0, 50, 60); // Dark Cyan
        palette[0x0D] = (0, 0, 0); // Black
        palette[0x0E] = (0, 0, 0); // Black
        palette[0x0F] = (0, 0, 0); // Black

        palette[0x10] = (152, 150, 152); // Medium Gray [Silver]
        palette[0x11] = (8, 76, 196); // Medium Azure
        palette[0x12] = (48, 50, 236); // Medium Blue [Blue]
        palette[0x13] = (92, 30, 228); // Medium Violet
        palette[0x14] = (136, 20, 176); // Medium Magenta [Purple]
        palette[0x15] = (160, 20, 100); // Medium Rose
        palette[0x16] = (152, 34, 32); // Medium Red/Maroon [Red]
        palette[0x17] = (120, 60, 0); // Medium Orange
        palette[0x18] = (84, 90, 0); // Medium Yellow/Olive [Olive]
        palette[0x19] = (40, 114, 0); // Medium Chartreuse
        palette[0x1A] = (8, 124, 0); // Medium Green [Green]
        palette[0x1B] = (0, 118, 40); // Medium Spring
        palette[0x1C] = (0, 102, 120); // Medium Cyan [Teal]
        palette[0x1D] = (0, 0, 0); // Black
        palette[0x1E] = (0, 0, 0); // Black
        palette[0x1F] = (0, 0, 0); // Black

        palette[0x20] = (236, 238, 236); // Light Gray [White]
        palette[0x21] = (76, 154, 236); // Light Azure
        palette[0x22] = (120, 124, 236); // Light Blue
        palette[0x23] = (176, 98, 236); // Light Violet
        palette[0x24] = (228, 84, 236); // Light Magenta [Fuchsia / Magenta]
        palette[0x25] = (236, 88, 180); // Light Rose
        palette[0x26] = (236, 106, 100); // Light Red/Maroon
        palette[0x27] = (212, 136, 32); // Light Orange
        palette[0x28] = (160, 170, 0); // Light Yellow [Yellow]
        palette[0x29] = (116, 196, 0); // Light Chartreuse
        palette[0x2A] = (76, 208, 32); // Light Green [Lime]
        palette[0x2B] = (56, 204, 108); // Light Spring
        palette[0x2C] = (56, 180, 204); // Light Cyan [Aqua / Cyan]
        palette[0x2D] = (60, 60, 60);
        palette[0x2E] = (0, 0, 0);
        palette[0x2F] = (0, 0, 0);

        palette[0x30] = (236, 238, 236); // Pale Gray White
        palette[0x31] = (168, 204, 236); // Pale Azure
        palette[0x32] = (188, 188, 236); // Pale Blue
        palette[0x33] = (212, 178, 236); // Pale Violet
        palette[0x34] = (236, 174, 236); // Pale Magenta
        palette[0x35] = (236, 174, 212); // Pale Rose
        palette[0x36] = (236, 180, 176); // Pale Red/Maroon
        palette[0x37] = (228, 196, 144); // Pale Orange
        palette[0x38] = (204, 210, 120); // Pale Yellow
        palette[0x39] = (180, 222, 120); // Pale Chartreuse
        palette[0x3A] = (168, 226, 144); // Pale Green
        palette[0x3B] = (152, 226, 180); // Pale Spring
        palette[0x3C] = (160, 214, 228); // Pale Cyan
        palette[0x3D] = (160, 162, 160);
        palette[0x3E] = (0, 0, 0);
        palette[0x3F] = (0, 0, 0);
        palette
    }

    pub fn cpu_read(&self, addr: u16) -> u8 {
        let mut _data = 0x00;

        match addr & 0x0F {
            // Control
            0x00 => {}
            // Mask
            0x01 => {}
            // Status
            0x02 => {}
            // OAM Address
            0x03 => {}
            // OAM Data
            0x04 => {}
            // Scroll
            0x05 => {}
            // PPU Address
            0x06 => {}
            // PPU Data
            0x07 => {}
            // Unknown
            _ => {
                unimplemented!()
            }
        }

        _data
    }

    pub fn cpu_write(&self, addr: u16, _data: u8) {
        match addr & 0x0F {
            // Control
            0x00 => {}
            // Mask
            0x01 => {}
            // Status
            0x02 => {}
            // OAM Address
            0x03 => {}
            // OAM Data
            0x04 => {}
            // Scroll
            0x05 => {}
            // PPU Address
            0x06 => {}
            // PPU Data
            0x07 => {}
            // Unknown
            _ => {
                unimplemented!()
            }
        }
    }

    pub fn _ppu_read(&self, addr: u16) -> u8 {
        let addr = addr & 0x3FFF;

        self.cartridge
            .as_ref()
            .unwrap()
            .read()
            .unwrap()
            .ppu_read(addr)
    }

    pub fn _ppu_write(&self, addr: u16, data: u8) {
        let addr = addr & 0x3FFF;

        self.cartridge
            .as_ref()
            .unwrap()
            .write()
            .unwrap()
            .ppu_write(addr, data);
    }

    pub fn clock(&mut self) -> bool {
        let mut frame_complete = false;
        self.cycle += 1;

        if self.cycle > 340 {
            self.cycle = 0;
            self.scanline += 1;

            if self.scanline > 260 {
                self.scanline = -1;
                frame_complete = true;
            }
        }

        if self.scanline >= 0
            && self.scanline < NES_RESOLUTION_HEIGHT as i16
            && self.cycle < NES_RESOLUTION_WIDTH as i16
        {
            let offset = self.cycle as u32 * 4 + (NES_RESOLUTION_WIDTH as u32 * self.scanline as u32 * 4);

            // Fill with random data at the moment
            let (mut r, mut g, mut b) = self.palette[0x00];
            if random() {
                (b, g, r) = self.palette[0x30];
            }

            self.screen[offset as usize] = b;
            self.screen[(offset + 1) as usize] = g;
            self.screen[(offset + 2) as usize] = r;
        }

        frame_complete
    }

    pub fn connect_cartridge(&mut self, cartridge: Arc<RwLock<Cartridge>>) {
        self.cartridge = Some(Arc::clone(&cartridge));
    }
}
