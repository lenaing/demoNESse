//! NES CPU Bus.

use super::{cartridge::Cartridge, ppu::PPU};
use std::sync::{Arc, RwLock};

/// NES CPU Bus.
///
/// # Nesdev Wiki
///
/// <https://www.nesdev.org/wiki/CPU_memory_map>
///
/// The CPU addresses a 16kB space.
///
/// |Address range|Size  |Description               |
/// |-------------|------|--------------------------|
/// |$0000-$07FF  |$0800 |2KB internal RAM          |
/// |$0800-$1FFF  |$0800 |Mirrors of $0000-$07FF    |
/// |$2000-$2007  |$0008 |NES PPU registers         |
/// |$2008-$3FFF  |$1FF8 |Mirrors of $2000-$2007    |
/// |$4000-$4017  |$0018 |NES APU and I/O registers |
/// |$4018-$401F  |$0008 |Unknown                   |
/// |$4020-$FFFF  |$BFE0 |Cartridge space           |
pub struct Bus {
    /// 2 KB of internal WRAM.
    wram: [u8; 2 * 1024],
    /// NES PPU.
    ppu: Arc<RwLock<PPU>>,
    /// Cartridge.
    cart: Option<Arc<RwLock<Cartridge>>>,
}

impl Bus {
    pub fn new(ppu: Arc<RwLock<PPU>>) -> Self {
        Self {
            wram: [0; 2 * 1024], // 2KB RAM
            ppu,
            cart: None,
        }
    }

    pub fn cpu_read(&self, addr: u16) -> u8 {
        let mut data = 0;

        if addr <= 0x1FFF {
            // Internal RAM
            data = self.wram[(addr & 0x07FF) as usize];
        } else if addr >= 0x2000 && addr <= 0x3FFF {
            // PPU
            data = self.ppu.read().unwrap().cpu_read(addr & 0x0007);
        } else if addr >= 0x4020 {
            // Cartridge
            data = self.cart.as_ref().unwrap().read().unwrap().cpu_read(addr);
        }

        data
    }

    pub fn cpu_write(&mut self, addr: u16, data: u8) {
        if addr <= 0x1FFF {
            // Internal RAM
            self.wram[(addr & 0x07FF) as usize] = data;
        } else if addr >= 0x2000 && addr <= 0x3FFF {
            // PPU
            self.ppu.write().unwrap().cpu_write(addr & 0x0007, data)
        } else if addr >= 0x4020 {
            // Cartridge
            self.cart
                .as_ref()
                .unwrap()
                .write()
                .unwrap()
                .cpu_write(addr, data);
        }
    }

    pub fn insert_cartridge(&mut self, cart: Arc<RwLock<Cartridge>>) {
        self.cart = Some(Arc::clone(&cart));
        self.ppu.write().unwrap().connect_cartridge(cart);
    }
}
