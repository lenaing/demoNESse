mod sdl2_frontend;
pub use sdl2_frontend::SDL2Frontend;

use super::ppu::ScreenData;

/// The `Frontend` trait define the required methods for a NES GUI.
pub trait Frontend {
    /// Handle user input.
    fn handle_input(&mut self) -> Message;

    /// Render the Video RAM state and display it to the user.
    fn render(&mut self, screen: &ScreenData);

    /// Display some runtime statistics.
    fn infos(&mut self, rom_title: &str, fps: u32);
}

pub enum Message {
    None,
    DebugToggle,
    DebugStep,
    DebugFrame,
    Reset,
    Quit,
}
