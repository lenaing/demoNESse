//! NES CPU, based on the MOS 6502 processor.
//!
//! Made by Ricoh, modified to disable Binary Coded Decimal Mode.
//!
//! Currently implements the RP2A03 Chip only, used in NTSC NES.
//!
//! Most information was implemented using these documentations :
//! * [MOS Technology 6502 Datasheet](https://archive.org/details/mos_6500_mpu_preliminary_may_1976)
//! * [Nesdev Wiki](https://www.nesdev.org/wiki/CPU)
//! * [OneLoneCoder olcNES](https://github.com/OneLoneCoder/olcNES) and his
//!     [inspirational video](https://www.youtube.com/watch?v=nViZg02IMQo).
use std::{
    collections::BTreeMap,
    sync::{Arc, RwLock},
};

mod instructions;

use super::bus::Bus;
use instructions::{AddressingMode, Operation};

/// Non-Maskable Interrupt Vector.
const NMI_VECTOR: u16 = 0xFFFA;
/// Reset Vector.
const RES_VECTOR: u16 = 0xFFFC;
/// Interrupt Request Vector.
const IRQ_VECTOR: u16 = 0xFFFE;

/// Processor status register flags.
///
/// # Nesdev Wiki
///
/// <https://www.nesdev.org/wiki/Status_flags>
pub enum StatusFlag {
    /// Carry.
    C = (1 << 0),
    /// Zero.
    Z = (1 << 1),
    /// Interrupt Disable.
    I = (1 << 2),
    /// Decimal Mode.
    D = (1 << 3),
    /// Break / B Flag.
    B = (1 << 4),
    /// Unused.
    U = (1 << 5),
    /// Overflow.
    V = (1 << 6),
    /// Negative.
    N = (1 << 7),
}

/// NES CPU.
pub struct CPU {
    /// Accumulator Register.
    a: u8,
    /// X Index Register.
    x: u8,
    /// Y Index Register.
    y: u8,
    /// Program Counter.
    pc: u16,
    /// Stack Pointer.
    s: u8,
    /// Status Register.
    p: u8,
    /// Current instruction opcode.
    opcode: u8,
    /// Current instruction data.
    fetched: u8,
    /// Absolute address to read data from for the current instruction.
    addr_abs: u16,
    /// Relative address to read data from for the current instruction.
    addr_rel: u16,
    /// Remaining cycles during wich the CPU is still busy.
    cycles: u8,
    bus: Arc<RwLock<Bus>>,
}

impl CPU {
    pub fn new(bus: Arc<RwLock<Bus>>) -> Self {
        // Cf https://www.nesdev.org/wiki/CPU_power_up_state#At_power-up
        Self {
            a: 0x00,
            x: 0x00,
            y: 0x00,
            pc: 0x0000,
            s: 0xFD,
            p: 0x00 | StatusFlag::I as u8 | StatusFlag::B as u8 | StatusFlag::U as u8, // 0x34

            fetched: 0x00,
            addr_abs: 0x0000,
            addr_rel: 0x0000,
            opcode: 0x00,
            cycles: 0x00,
            bus,
        }
    }

    /// Read an 8-bit byte from the data bus, located at the specified 16-bit address.
    pub fn read(&self, addr: u16) -> u8 {
        self.bus.read().unwrap().cpu_read(addr)
    }

    /// Write a byte to the bus at the specified address.
    pub fn write(&self, addr: u16, data: u8) {
        self.bus.write().unwrap().cpu_write(addr, data);
    }

    /// Check if the value of a specific bit of the status register is set.
    fn get_flag(&self, flag: StatusFlag) -> bool {
        self.p & flag as u8 > 0
    }

    /// Sets or clear a specific bit of the status register.
    fn set_flag(&mut self, flag: StatusFlag, value: bool) {
        if value {
            self.p |= flag as u8;
        } else {
            self.p &= !(flag as u8);
        }
    }

    /// Perform one clock cycle of emulation.
    ///
    /// Each instruction requires a variable number of clock cycles to execute:
    ///
    /// * Fetching opcode
    /// * Fetching data Address
    /// * Fetching data from memory
    /// * Write modified data back to memory
    ///
    /// The 6502 implements some kind of pipeline but not a deep one and instruction does not
    /// overlap by more than one clock.
    ///
    /// IRQ and NMI interrupts wait for instruction completion before being executed.
    ///
    /// Rather than implementing microprocessor microcode for each of the cycles, perform
    /// entire computation in one cycle and sleep for the rest to respect timings.
    pub fn clock(&mut self) {
        if self.cycles == 0 {
            // Read next instruction byte
            self.opcode = self.read(self.pc);
            self.pc += 1;

            // Get instruction implementation information
            let instruction = instructions::get(self.opcode);

            // Initial number of cycles for instruction execution
            self.cycles = instruction.cycles();

            // Some operations are page boundary sensitive
            let page_boundary_crossed_sensitive_operation = match instruction.op() {
                Operation::ADC
                | Operation::AND
                | Operation::CMP
                | Operation::EOR
                | Operation::LDA
                | Operation::LDX
                | Operation::LDY
                | Operation::SBC => true,
                _ => false,
            };

            // Grab address to read for operation and check if page boundary is crossed
            let page_boundary_crossed = match instruction.am() {
                AddressingMode::ACC => self.addressing_mode_acc(),
                AddressingMode::IMM => self.addressing_mode_imm(),
                AddressingMode::ABS => self.addressing_mode_abs(),
                AddressingMode::ZP0 => self.addressing_mode_zp0(),
                AddressingMode::ZPX => self.addressing_mode_zpx(),
                AddressingMode::ZPY => self.addressing_mode_zpy(),
                AddressingMode::ABX => self.addressing_mode_abx(),
                AddressingMode::ABY => self.addressing_mode_aby(),
                AddressingMode::IMP => self.addressing_mode_imp(),
                AddressingMode::REL => self.addressing_mode_rel(),
                AddressingMode::IZX => self.addressing_mode_izx(),
                AddressingMode::IZY => self.addressing_mode_izy(),
                AddressingMode::IND => self.addressing_mode_ind(),
            };

            // Execute Operation
            match instruction.op() {
                Operation::ADC => self.execute_adc(),
                Operation::AND => self.execute_and(),
                Operation::ASL => self.execute_asl(),
                Operation::BCC => self.execute_bcc(),
                Operation::BCS => self.execute_bcs(),
                Operation::BEQ => self.execute_beq(),
                Operation::BIT => self.execute_bit(),
                Operation::BMI => self.execute_bmi(),
                Operation::BNE => self.execute_bne(),
                Operation::BPL => self.execute_bpl(),
                Operation::BRK => self.execute_brk(),
                Operation::BVC => self.execute_bvc(),
                Operation::BVS => self.execute_bvs(),
                Operation::CLC => self.execute_clc(),
                Operation::CLD => self.execute_cld(),
                Operation::CLI => self.execute_cli(),
                Operation::CLV => self.execute_clv(),
                Operation::CMP => self.execute_cmp(),
                Operation::CPX => self.execute_cpx(),
                Operation::CPY => self.execute_cpy(),
                Operation::DEC => self.execute_dec(),
                Operation::DEX => self.execute_dex(),
                Operation::DEY => self.execute_dey(),
                Operation::EOR => self.execute_eor(),
                Operation::INC => self.execute_inc(),
                Operation::INX => self.execute_inx(),
                Operation::INY => self.execute_iny(),
                Operation::JMP => self.execute_jmp(),
                Operation::JSR => self.execute_jsr(),
                Operation::LDA => self.execute_lda(),
                Operation::LDX => self.execute_ldx(),
                Operation::LDY => self.execute_ldy(),
                Operation::LSR => self.execute_lsr(),
                Operation::NOP => self.execute_nop(),
                Operation::ORA => self.execute_ora(),
                Operation::PHA => self.execute_pha(),
                Operation::PHP => self.execute_php(),
                Operation::PLA => self.execute_pla(),
                Operation::PLP => self.execute_plp(),
                Operation::ROL => self.execute_rol(),
                Operation::ROR => self.execute_ror(),
                Operation::RTI => self.execute_rti(),
                Operation::RTS => self.execute_rts(),
                Operation::SBC => self.execute_sbc(),
                Operation::SEC => self.execute_sec(),
                Operation::SED => self.execute_sed(),
                Operation::SEI => self.execute_sei(),
                Operation::STA => self.execute_sta(),
                Operation::STX => self.execute_stx(),
                Operation::STY => self.execute_sty(),
                Operation::TAX => self.execute_tax(),
                Operation::TAY => self.execute_tay(),
                Operation::TSX => self.execute_tsx(),
                Operation::TXA => self.execute_txa(),
                Operation::TXS => self.execute_txs(),
                Operation::TYA => self.execute_tya(),
                Operation::XXX => self.execute_xxx(),
            };

            // Add any additionnal cycles required to execute this operation
            if page_boundary_crossed_sensitive_operation & page_boundary_crossed {
                self.cycles += 1;
            }
        }

        self.cycles -= 1;
    }

    //=============================================================================================
    // Accessors
    //=============================================================================================
    /// Accumulator Register.
    pub fn a(&self) -> u8 {
        self.a
    }

    /// X Index Register.
    pub fn x(&self) -> u8 {
        self.x
    }

    /// Y Index Register.
    pub fn y(&self) -> u8 {
        self.y
    }

    /// Program Counter.
    pub fn pc(&self) -> u16 {
        self.pc
    }

    /// Stack Pointer Register.
    pub fn s(&self) -> u8 {
        self.s
    }

    /// Status Register.
    pub fn p(&self) -> u8 {
        self.p
    }

    //=============================================================================================
    // Addressing Modes
    // Cf MOS Technology 6502 Datasheet > Common Characteristics > Addressing Modes
    //=============================================================================================

    /// Accumulator Addressing.
    ///
    /// # MOS Technology 6502 Datasheet
    ///
    /// > This form of addressing is represented with a one byte instruction, implying an operation
    /// on the accumulator.
    fn addressing_mode_acc(&mut self) -> bool {
        self.fetched = self.a;
        false
    }

    /// Immediate Addressing.
    ///
    /// # MOS Technology 6502 Datasheet
    ///
    /// > In immediate addressing, the operand is contained in the second byte of the instruction,
    /// with no further memory addressing required.
    fn addressing_mode_imm(&mut self) -> bool {
        self.addr_abs = self.pc;
        self.pc += 1;
        false
    }

    /// Absolute Addressing.
    ///
    /// # MOS Technology 6502 Datasheet
    ///
    /// > In absolute Addressing, the second byte specifies the eight low order bits of the
    /// effective address while the third byte specifies the eight high order bits. Thus, the
    /// absolute addressing mode allows access to the entire 65K bytes of addressable memory.
    fn addressing_mode_abs(&mut self) -> bool {
        let lo = self.read(self.pc) as u16;
        self.pc += 1;
        let hi = self.read(self.pc) as u16;
        self.pc += 1;
        self.addr_abs = (hi << 8) | lo;
        false
    }

    /// Zero Page Addressing.
    ///
    /// # MOS Technology 6502 Datasheet
    ///
    /// > The zero page instructions allow for shorter code and execution times by only fetching
    /// the second byte of the instruction and assuming a zero high adress byte. Careful use of the
    /// zero page can result in significant increase in code efficiency.
    fn addressing_mode_zp0(&mut self) -> bool {
        self.addr_abs = self.read(self.pc) as u16;
        self.pc += 1;
        self.addr_abs &= 0x00FF;
        false
    }

    /// Indexed Zero Page Addressing (X indexing).
    ///
    /// # MOS Technology 6502 Datasheet
    ///
    /// > This form of addressing is used in conjunction with the index register and is referred to
    /// as "Zero Page, X" or "Zero Page, Y". The effective address is calculated by adding the
    /// second byte to the contents of the index register. Since this is a form of "Zero Page"
    /// addressing, the content of the second byte references a location in page zero.
    /// Additionnally due to the "Zero Page" addressing nature of this mode, no carry is added to
    /// the high order 8 bits of memory and crossing of page boundaries does not occur.
    fn addressing_mode_zpx(&mut self) -> bool {
        self.addr_abs = self.read(self.pc) as u16 + self.x as u16;
        self.pc += 1;
        self.addr_abs &= 0x00FF;
        false
    }

    /// Indexed Zero Page Addressing (Y indexing).
    ///
    /// Cf [CPU::addressing_mode_zpx]
    fn addressing_mode_zpy(&mut self) -> bool {
        self.addr_abs = self.read(self.pc) as u16 + self.y as u16;
        self.pc += 1;
        self.addr_abs &= 0x00FF;
        false
    }

    /// Indexed Absolute Addressing (X Indexing).
    ///
    /// # MOS Technology 6502 Datasheet
    ///
    /// > This form of addressing is used in conjunction with X and Y index register and is
    /// referred to "Absolute, X" and "Absolute, Y". The effective address is formed by adding the
    /// contents of X or Y to the address contained in the second and third bytes of the
    /// instruction. This mode allows the index register to contain the index or count value and
    /// the instruction to contain the base address. This type of indexing allows any location
    /// referencing and the index to modify multiple fields resulting in reduced coding and
    /// execution time.
    fn addressing_mode_abx(&mut self) -> bool {
        let lo = self.read(self.pc) as u16;
        self.pc += 1;
        let hi = self.read(self.pc) as u16;
        self.pc += 1;
        self.addr_abs = (hi << 8) | lo;
        self.addr_abs += self.x as u16;

        // We may have crossed a page boundary
        (self.addr_abs & 0xFF00) != (hi << 8)
    }

    /// Indexed Absolute Addressing (Y Indexing).
    ///
    /// Cf [CPU::addressing_mode_abx]
    fn addressing_mode_aby(&mut self) -> bool {
        let lo = self.read(self.pc) as u16;
        self.pc += 1;
        let hi = self.read(self.pc) as u16;
        self.pc += 1;
        self.addr_abs = (hi << 8) | lo;
        self.addr_abs += self.y as u16;

        // We may have crossed a page boundary
        (self.addr_abs & 0xFF00) != (hi << 8)
    }

    /// Implied Addressing.
    ///
    /// # MOS Technology 6502 Datasheet
    ///
    /// > In the implied addressing mode, the address containing the operand is implicitly stated
    /// in the operation code of the instruction.
    fn addressing_mode_imp(&mut self) -> bool {
        false
    }

    /// Relative Addressing.
    ///
    /// # MOS Technology 6502 Datasheet
    ///
    /// > Relative addressing is used only with branch instructions and establishes a destination
    /// for the conditional branch. The second byte of the instruction becomes the operand which is
    /// an "Offset" added to the contents of the lower eight bits of the program counter when the
    /// counter is set at the next instruction. The range of the offset is -128 to +127 bytes from
    /// the next instruction.
    fn addressing_mode_rel(&mut self) -> bool {
        self.addr_rel = self.read(self.pc) as u16;
        self.pc += 1;

        // Preserve sign as we read an u8 to an u16
        if self.addr_rel & 0x80 > 0 {
            self.addr_rel |= 0xFF00;
        }
        false
    }

    /// Indexed Indirect Addressing.
    ///
    /// # MOS Technology 6502 Datasheet
    ///
    /// > In indexed indirect addressing (referred to as (Indirect, X)), the second byte of the
    /// instruction is added to the contents of the X index register, discarding the carry. The
    /// result of this addition points to a memory location on page zero whose contents is the low
    /// order eight bits of the effective address. The next memory location in page zero contains
    /// the high order eight bits of the effective address. Both memory locations specifying the
    /// high and low order bytes of the effective address must be in page zero.
    fn addressing_mode_izx(&mut self) -> bool {
        let t = self.read(self.pc) as u16;
        self.pc += 1;

        // No check for page 0 location ?
        let lo = self.read(self.x as u16 + t) as u16;
        let hi = self.read(self.x as u16 + t + 1) as u16;

        self.addr_abs = (hi << 8) | lo;
        false
    }

    /// Indirect Indexed Addressing.
    ///
    /// # MOS Technology 6502 Datasheet
    ///
    /// > In indirect indexed addressing (referred to as (Indirect), Y), the second byte of the
    /// instruction points to a memory location in page zero. The contents of this memory location
    /// is added to the contents of the Y index register, the result being the low order eight bits
    /// of the effective address. The carry from this addition is added to the contents of the next
    /// page zero memory location, the result being the high order eight bits of the effective
    /// address.
    fn addressing_mode_izy(&mut self) -> bool {
        let t = self.read(self.pc) as u16;
        self.pc += 1;

        let lo = self.read(t) as u16;
        let hi = self.read(t + 1) as u16;

        self.addr_abs = (hi << 8) | lo;
        self.addr_abs += self.y as u16;

        // We may have crossed a page boundary
        self.addr_abs & 0xFF00 != (hi << 8)
    }

    /// Absolute Indirect Addressing.
    ///
    /// # MOS Technology 6502 Datasheet
    ///
    /// > The second byte of the instruction contains the low order eight bits of a memory
    /// location. The high order eight bits ot that memory location is contained in the third byte
    /// of the instruction. The contents of the fully specified memory location is the low order
    /// byte of the effective address. The next memory location contains the high order byte of the
    /// effective address which is loaded into the sixteen bits of the program counter.
    fn addressing_mode_ind(&mut self) -> bool {
        let ptr_lo = self.read(self.pc) as u16;
        self.pc += 1;
        let ptr_hi = self.read(self.pc) as u16;
        self.pc += 1;

        let ptr = (ptr_hi << 8) | ptr_lo;

        let lo = self.read(ptr) as u16;

        let hi = if ptr_lo == 0x00FF {
            // Simulate page boundary hardware bug
            self.read(ptr & 0xFF00) as u16
        } else {
            self.read(ptr + 1) as u16
        };

        self.addr_abs = (hi << 8) | lo;
        false
    }

    //=============================================================================================
    // Instructions
    //=============================================================================================

    /// Fetch operand from a predetermined addressed.
    fn fetch(&mut self) {
        self.fetched = match instructions::get(self.opcode).am() {
            AddressingMode::ACC => self.a,
            AddressingMode::IMP => self.fetched,
            _ => self.read(self.addr_abs),
        };
    }

    /// Add Memory to Accumulator with Carry.
    ///
    /// |Mnemonic|Operation    |Condition Codes|
    /// |--------|-------------|---------------|
    /// |ADC     |A = A + M + C|N, Z, C, V     |
    ///
    fn execute_adc(&mut self) {
        self.fetch();
        let temp = self.a as u16 + self.fetched as u16 + self.get_flag(StatusFlag::C) as u16;

        // Carry flag if value over 0xFF
        self.set_flag(StatusFlag::C, temp > 255);
        // Zero flag if result is 0
        self.set_flag(StatusFlag::Z, (temp & 0x00FF) == 0);
        // Overflow when... well.
        self.set_flag(
            StatusFlag::V,
            (!((self.a as u16 ^ self.fetched as u16) & (self.a as u16 ^ temp)) & 0x0080) > 0,
        );
        // Negative flag if the MSB of the result is set.
        self.set_flag(StatusFlag::N, temp & 0x80 > 0);

        self.a = (temp & 0x00FF) as u8;
    }

    /// "AND" Memory with Accumulator.
    ///
    /// |Mnemonic|Operation    |Condition Codes|
    /// |--------|-------------|---------------|
    /// |AND     |A = A & M    |N, Z           |
    ///
    fn execute_and(&mut self) {
        self.fetch();
        self.a &= self.fetched;
        self.set_flag(StatusFlag::Z, self.a == 0x00);
        self.set_flag(StatusFlag::N, self.a & 0x80 > 0);
    }

    /// Shift Left One Bit (Memory or Accumulator).
    ///
    /// |Mnemonic|Operation             |Condition Codes|
    /// |--------|----------------------|---------------|
    /// |ASL     |A = C <- (A << 1) <- 0|N, Z, C        |
    ///
    fn execute_asl(&mut self) {
        self.fetch();
        let temp = (self.fetched as u16) << 1;
        self.set_flag(StatusFlag::C, (temp & 0xFF00) > 0);
        self.set_flag(StatusFlag::Z, (temp & 0x00FF) == 0);
        self.set_flag(StatusFlag::N, temp & 0x80 > 0);

        let instruction = instructions::get(self.opcode);
        let value = (temp & 0x00FF) as u8;
        match instruction.am() {
            AddressingMode::ACC => self.a = value,
            _ => self.write(self.addr_abs, value),
        };
    }

    /// Branch on Carry Clear.
    ///
    /// |Mnemonic|Operation               |Condition Codes|
    /// |--------|------------------------|---------------|
    /// |BCC     |if (C == 0) pc = address|               |
    ///
    fn execute_bcc(&mut self) {
        if self.get_flag(StatusFlag::C) == false {
            self.cycles += 1;
            self.addr_abs = self.pc + self.addr_rel;

            if self.addr_abs & 0xFF00 != self.pc & 0xFF00 {
                // Branch cross page boundary
                self.cycles += 1;
            }

            self.pc = self.addr_abs;
        }
    }

    /// Branch on Carry Set.
    ///
    /// |Mnemonic|Operation               |Condition Codes|
    /// |--------|------------------------|---------------|
    /// |BCS     |if (C == 1) pc = address|               |
    ///
    fn execute_bcs(&mut self) {
        if self.get_flag(StatusFlag::C) {
            self.cycles += 1;
            self.addr_abs = self.pc + self.addr_rel;

            if self.addr_abs & 0xFF00 != self.pc & 0xFF00 {
                // Branch cross page boundary
                self.cycles += 1;
            }

            self.pc = self.addr_abs;
        }
    }

    /// Branch on Result Zero.
    ///
    /// |Mnemonic|Operation               |Condition Codes|
    /// |--------|------------------------|---------------|
    /// |BEQ     |if (Z == 1) pc = address|               |
    ///
    fn execute_beq(&mut self) {
        if self.get_flag(StatusFlag::Z) {
            self.cycles += 1;
            self.addr_abs = self.pc + self.addr_rel;

            if self.addr_abs & 0xFF00 != self.pc & 0xFF00 {
                // Branch cross page boundary
                self.cycles += 1;
            }

            self.pc = self.addr_abs;
        }
    }

    /// Test Bits in Memory with Accumulator.
    ///
    /// |Mnemonic|Operation               |Condition Codes|
    /// |--------|------------------------|---------------|
    /// |BIT     |if (Z == 1) pc = address|N, Z, V        |
    ///
    fn execute_bit(&mut self) {
        self.fetch();
        self.set_flag(StatusFlag::Z, ((self.a & self.fetched) & 0x00FF) == 0);
        self.set_flag(StatusFlag::V, self.fetched & (1 << 6) > 0);
        self.set_flag(StatusFlag::N, self.fetched & (1 << 7) > 0);
    }

    /// Branch on Result Minus.
    ///
    /// |Mnemonic|Operation               |Condition Codes|
    /// |--------|------------------------|---------------|
    /// |BMI     |if (N == 1) pc = address|               |
    ///
    fn execute_bmi(&mut self) {
        if self.get_flag(StatusFlag::N) {
            self.cycles += 1;
            self.addr_abs = self.pc + self.addr_rel;

            if self.addr_abs & 0xFF00 != self.pc & 0xFF00 {
                // Branch cross page boundary
                self.cycles += 1;
            }

            self.pc = self.addr_abs;
        }
    }

    /// Branch on Result not Zero.
    ///
    /// |Mnemonic|Operation               |Condition Codes|
    /// |--------|------------------------|---------------|
    /// |BNE     |if (Z == 0) pc = address|               |
    ///
    fn execute_bne(&mut self) {
        if self.get_flag(StatusFlag::Z) == false {
            self.cycles += 1;
            self.addr_abs = self.pc.wrapping_add(self.addr_rel);

            if self.addr_abs & 0xFF00 != self.pc & 0xFF00 {
                // Branch cross page boundary
                self.cycles += 1;
            }

            self.pc = self.addr_abs;
        }
    }

    /// Branch on Result Plus.
    ///
    /// |Mnemonic|Operation               |Condition Codes|
    /// |--------|------------------------|---------------|
    /// |BPL     |if (N == 0) pc = address|               |
    ///
    fn execute_bpl(&mut self) {
        if self.get_flag(StatusFlag::N) == false {
            self.cycles += 1;
            self.addr_abs = self.pc.wrapping_add(self.addr_rel);

            if self.addr_abs & 0xFF00 != self.pc & 0xFF00 {
                // Branch cross page boundary
                self.cycles += 1;
            }

            self.pc = self.addr_abs;
        }
    }

    /// Force Break.
    ///
    /// |Mnemonic|Operation               |Condition Codes|
    /// |--------|------------------------|---------------|
    /// |BRK     |Software interrupt      | I             |
    ///
    fn execute_brk(&mut self) {
        self.pc += 1;
        self.interrupt(IRQ_VECTOR, true);
    }

    /// Branch on Overflow Clear.
    ///
    /// |Mnemonic|Operation               |Condition Codes|
    /// |--------|------------------------|---------------|
    /// |BVC     |if (V == 0) pc = address|               |
    ///
    fn execute_bvc(&mut self) {
        if self.get_flag(StatusFlag::V) == false {
            self.cycles += 1;
            self.addr_abs = self.pc + self.addr_rel;

            if self.addr_abs & 0xFF00 != self.pc & 0xFF00 {
                // Branch cross page boundary
                self.cycles += 1;
            }

            self.pc = self.addr_abs;
        }
    }

    /// Branch on Overflow Set.
    ///
    /// |Mnemonic|Operation               |Condition Codes|
    /// |--------|------------------------|---------------|
    /// |BVS     |if (V == 1) pc = address|               |
    ///
    fn execute_bvs(&mut self) {
        if self.get_flag(StatusFlag::V) {
            self.cycles += 1;
            self.addr_abs = self.pc + self.addr_rel;

            if self.addr_abs & 0xFF00 != self.pc & 0xFF00 {
                // Branch cross page boundary
                self.cycles += 1;
            }

            self.pc = self.addr_abs;
        }
    }

    /// Clear Carry Flag.
    ///
    /// |Mnemonic|Operation               |Condition Codes|
    /// |--------|------------------------|---------------|
    /// |CLC     |C = 0                   |C              |
    ///
    fn execute_clc(&mut self) {
        self.set_flag(StatusFlag::C, false);
    }

    /// Clear Decimal Mode.
    ///
    /// |Mnemonic|Operation               |Condition Codes|
    /// |--------|------------------------|---------------|
    /// |CLD     |D = 0                   |D              |
    ///
    fn execute_cld(&mut self) {
        self.set_flag(StatusFlag::D, false);
    }

    /// Clear Interrupt Disable Bit.
    ///
    /// |Mnemonic|Operation               |Condition Codes|
    /// |--------|------------------------|---------------|
    /// |CLI     |I = 0                   |I              |
    ///
    fn execute_cli(&mut self) {
        self.set_flag(StatusFlag::I, false);
    }

    /// Clear Overflow Flag.
    ///
    /// |Mnemonic|Operation               |Condition Codes|
    /// |--------|------------------------|---------------|
    /// |CLV     |V = 0                   |V              |
    ///
    fn execute_clv(&mut self) {
        self.set_flag(StatusFlag::V, false);
    }

    /// Compare Memory and Accumulator.
    ///
    /// |Mnemonic|Operation                     |Condition Codes|
    /// |--------|------------------------------|---------------|
    /// |CMP     |C <- A >= M, Z <- (A - M) == 0|N, Z, C        |
    ///
    fn execute_cmp(&mut self) {
        self.fetch();
        let temp = self.a as u16 - self.fetched as u16;
        self.set_flag(StatusFlag::C, self.a >= self.fetched);
        self.set_flag(StatusFlag::Z, (temp & 0x00FF) == 0);
        self.set_flag(StatusFlag::N, temp & 0x80 > 0);
    }

    /// Compare Memory and Index X.
    ///
    /// |Mnemonic|Operation                     |Condition Codes|
    /// |--------|------------------------------|---------------|
    /// |CPX     |C <- X >= M, Z <- (X - M) == 0|N, Z, C        |
    ///
    fn execute_cpx(&mut self) {
        self.fetch();
        let temp = self.x as u16 - self.fetched as u16;
        self.set_flag(StatusFlag::C, self.x >= self.fetched);
        self.set_flag(StatusFlag::Z, (temp & 0x00FF) == 0);
        self.set_flag(StatusFlag::N, temp & 0x80 > 0);
    }

    /// Compare Memory and Index Y.
    ///
    /// |Mnemonic|Operation                     |Condition Codes|
    /// |--------|------------------------------|---------------|
    /// |CPY     |C <- Y >= M, Z <- (Y - M) == 0|N, Z, C        |
    ///
    fn execute_cpy(&mut self) {
        self.fetch();
        let temp = self.y as u16 - self.fetched as u16;
        self.set_flag(StatusFlag::C, self.y >= self.fetched);
        self.set_flag(StatusFlag::Z, (temp & 0x00FF) == 0);
        self.set_flag(StatusFlag::N, temp & 0x80 > 0);
    }

    /// Decrement Memory by One.
    ///
    /// |Mnemonic|Operation                     |Condition Codes|
    /// |--------|------------------------------|---------------|
    /// |DEC     |M = M - 1                     |N, Z           |
    ///
    fn execute_dec(&mut self) {
        self.fetch();
        let temp = self.fetched as u16 - 1;
        self.write(self.addr_abs, temp as u8 & 0x00FF);
        self.set_flag(StatusFlag::Z, (temp & 0x00FF) == 0);
        self.set_flag(StatusFlag::N, temp & 0x80 > 0);
    }

    /// Decrement Index X by One.
    ///
    /// |Mnemonic|Operation                     |Condition Codes|
    /// |--------|------------------------------|---------------|
    /// |DEX     |M = M - 1                     |N, Z           |
    ///
    fn execute_dex(&mut self) {
        self.x -= 1;
        self.set_flag(StatusFlag::Z, self.x == 0);
        self.set_flag(StatusFlag::N, self.x & 0x80 > 0);
    }

    /// Decrement Index Y by One.
    ///
    /// |Mnemonic|Operation                     |Condition Codes|
    /// |--------|------------------------------|---------------|
    /// |DEY     |M = M - 1                     |N, Z           |
    ///
    fn execute_dey(&mut self) {
        self.y -= 1;
        self.set_flag(StatusFlag::Z, self.y == 0);
        self.set_flag(StatusFlag::N, self.y & 0x80 > 0);
    }

    /// "Exclusive-or" Memory with Accumulator.
    ///
    /// |Mnemonic|Operation                     |Condition Codes|
    /// |--------|------------------------------|---------------|
    /// |EOR     |A = A ^ M                     |N, Z           |
    ///
    fn execute_eor(&mut self) {
        self.fetch();
        self.a ^= self.fetched;
        self.set_flag(StatusFlag::Z, self.a == 0);
        self.set_flag(StatusFlag::N, self.a & 0x80 > 0);
    }

    /// Increment Memory by One.
    ///
    /// |Mnemonic|Operation                     |Condition Codes|
    /// |--------|------------------------------|---------------|
    /// |INC     |M = M + 1                     |N, Z           |
    ///
    fn execute_inc(&mut self) {
        self.fetch();
        let temp = self.fetched as u16 + 1;
        self.write(self.addr_abs, temp as u8 & 0x00FF);
        self.set_flag(StatusFlag::Z, (temp & 0x00FF) == 0);
        self.set_flag(StatusFlag::N, temp & 0x80 > 0);
    }

    /// Increment Index X by One.
    ///
    /// |Mnemonic|Operation                     |Condition Codes|
    /// |--------|------------------------------|---------------|
    /// |INX     |X = X + 1                     |N, Z           |
    ///
    fn execute_inx(&mut self) {
        self.x += 1;
        self.set_flag(StatusFlag::Z, self.x == 0);
        self.set_flag(StatusFlag::N, self.x & 0x80 > 0);
    }

    /// Increment Index Y by One.
    ///
    /// |Mnemonic|Operation                     |Condition Codes|
    /// |--------|------------------------------|---------------|
    /// |INY     |Y = Y + 1                     |N, Z           |
    ///
    fn execute_iny(&mut self) {
        self.y += 1;
        self.set_flag(StatusFlag::Z, self.y == 0);
        self.set_flag(StatusFlag::N, self.y & 0x80 > 0);
    }

    /// Jump to New Location.
    ///
    /// |Mnemonic|Operation                     |Condition Codes|
    /// |--------|------------------------------|---------------|
    /// |JMP     |PC = addr                     |               |
    ///
    fn execute_jmp(&mut self) {
        self.pc = self.addr_abs;
    }

    /// Jump to New Location Saving Return Address.
    ///
    /// |Mnemonic|Operation                     |Condition Codes|
    /// |--------|------------------------------|---------------|
    /// |JSR     |SP <- PC, PC = addr           |               |
    ///
    fn execute_jsr(&mut self) {
        self.pc -= 1;
        self.write(0x0100 + self.s as u16, ((self.pc >> 8) & 0x00FF) as u8);
        self.s -= 1;
        self.write(0x0100 + self.s as u16, (self.pc & 0x00FF) as u8);
        self.s -= 1;
        self.pc = self.addr_abs;
    }

    /// Load Accumulator with Memory.
    ///
    /// |Mnemonic|Operation                     |Condition Codes|
    /// |--------|------------------------------|---------------|
    /// |LDA     |A = M                         |N, Z           |
    ///
    fn execute_lda(&mut self) {
        self.fetch();
        self.a = self.fetched;
        self.set_flag(StatusFlag::Z, self.a == 0);
        self.set_flag(StatusFlag::N, self.a & 0x80 > 0);
    }

    /// Load Index X with Memory.
    ///
    /// |Mnemonic|Operation                     |Condition Codes|
    /// |--------|------------------------------|---------------|
    /// |LDX     |X = M                         |N, Z           |
    ///
    fn execute_ldx(&mut self) {
        self.fetch();
        self.x = self.fetched;
        self.set_flag(StatusFlag::Z, self.x == 0);
        self.set_flag(StatusFlag::N, self.x & 0x80 > 0);
    }

    /// Load Index Y with Memory.
    ///
    /// |Mnemonic|Operation                     |Condition Codes|
    /// |--------|------------------------------|---------------|
    /// |LDY     |Y = M                         |N, Z           |
    ///
    fn execute_ldy(&mut self) {
        self.fetch();
        self.y = self.fetched;
        self.set_flag(StatusFlag::Z, self.y == 0);
        self.set_flag(StatusFlag::N, self.y & 0x80 > 0);
    }

    /// Shift One Bit Right (Memory or Accumulator).
    ///
    /// |Mnemonic|Operation                     |Condition Codes|
    /// |--------|------------------------------|---------------|
    /// |LSR     |A = C -> (A >> 1) -> 0        |N, Z, C        |
    ///
    fn execute_lsr(&mut self) {
        self.fetch();
        self.set_flag(StatusFlag::C, self.fetched & 0x0001 > 0);
        let temp = self.fetched as u16 >> 1;
        self.set_flag(StatusFlag::Z, temp & 0x00FF == 0);
        self.set_flag(StatusFlag::N, temp & 0x80 > 0);

        let instruction = instructions::get(self.opcode);
        let value = (temp & 0x00FF) as u8;
        match instruction.am() {
            AddressingMode::ACC => self.a = value,
            _ => self.write(self.addr_abs, value),
        };
    }

    /// No Operation.
    ///
    /// |Mnemonic|Operation                     |Condition Codes|
    /// |--------|------------------------------|---------------|
    ///
    fn execute_nop(&mut self) {}

    /// "OR" Memory with Accumulator.
    ///
    /// |Mnemonic|Operation                     |Condition Codes|
    /// |--------|------------------------------|---------------|
    /// |PHP     |A = A \| M                    |N, Z           |
    ///
    fn execute_ora(&mut self) {
        self.fetch();
        self.a |= self.fetched;
        self.set_flag(StatusFlag::Z, self.a == 0);
        self.set_flag(StatusFlag::N, self.a & 0x80 > 0);
    }

    /// Push Accumulator on Stack.
    ///
    /// |Mnemonic|Operation                     |Condition Codes|
    /// |--------|------------------------------|---------------|
    /// |PHA     |SP <- A                      |               |
    ///
    fn execute_pha(&mut self) {
        self.write(0x0100 + self.s as u16, self.a);
        self.s -= 1;
    }

    /// Push Processor Status on Stack.
    ///
    /// |Mnemonic|Operation                     |Condition Codes|
    /// |--------|------------------------------|---------------|
    /// |PHP     |SP <- P                      |B, U           |
    ///
    fn execute_php(&mut self) {
        self.write(
            0x0100 + self.s as u16,
            self.p | StatusFlag::B as u8 | StatusFlag::U as u8,
        );
        self.set_flag(StatusFlag::B, false);
        self.set_flag(StatusFlag::U, false);
        self.s -= 1;
    }

    /// Pull Accumulator from Stack.
    ///
    /// |Mnemonic|Operation                     |Condition Codes|
    /// |--------|------------------------------|---------------|
    /// |PLA     |A <- SP                      |N, Z           |
    ///
    fn execute_pla(&mut self) {
        self.s += 1;
        self.a = self.read(0x0100 + self.s as u16);
        self.set_flag(StatusFlag::Z, self.a == 0x00);
        self.set_flag(StatusFlag::N, self.a & 0x80 > 0);
    }

    /// Pull Processor Status from Stack.
    ///
    /// |Mnemonic|Operation                     |Condition Codes|
    /// |--------|------------------------------|---------------|
    /// |PLP     |P <- SP                      |               |
    ///
    fn execute_plp(&mut self) {
        self.s += 1;
        self.p = self.read(0x0100 + self.s as u16);
        self.set_flag(StatusFlag::B, true);
        self.set_flag(StatusFlag::U, true);
    }

    /// Rotate One Bit Left (Memory or Accumulator).
    ///
    /// |Mnemonic|Operation                     |Condition Codes|
    /// |--------|------------------------------|---------------|
    /// |ROL     |A = C <- (A << 1) <- C        |N, Z, C        |
    ///
    fn execute_rol(&mut self) {
        self.fetch();

        let lo = self.get_flag(StatusFlag::C) as u16;
        let hi = self.fetched as u16;
        let temp = (hi << 1) | lo;
        self.set_flag(StatusFlag::C, temp & 0xFF00 > 0);
        self.set_flag(StatusFlag::Z, (temp & 0x00FF) == 0x00);
        self.set_flag(StatusFlag::N, temp & 0x80 > 0);
        let value = (temp & 0x00FF) as u8;
        let instruction = instructions::get(self.opcode);
        match instruction.am() {
            AddressingMode::ACC => self.a = value,
            _ => self.write(self.addr_abs, value),
        };
    }

    /// Rotate One Bit Right (Memory or Accumulator).
    ///
    /// |Mnemonic|Operation                     |Condition Codes|
    /// |--------|------------------------------|---------------|
    /// |ROR     |A = C -> (A >> 1) -> C        |N, Z, C        |
    ///
    fn execute_ror(&mut self) {
        self.fetch();
        let lo = self.fetched as u16 >> 1;
        let hi = self.get_flag(StatusFlag::C) as u16;
        let temp = (hi << 7) | lo;
        self.set_flag(StatusFlag::C, self.fetched & 0x01 > 0);
        self.set_flag(StatusFlag::Z, (temp & 0x00FF) == 0x00);
        self.set_flag(StatusFlag::N, temp & 0x80 > 0);
        let value = (temp & 0x00FF) as u8;
        let instruction = instructions::get(self.opcode);
        match instruction.am() {
            AddressingMode::ACC => self.a = value,
            _ => self.write(self.addr_abs, value),
        };
    }

    /// Return from Interrupt.
    ///
    /// |Mnemonic|Operation                     |Condition Codes|
    /// |--------|------------------------------|---------------|
    /// |RTI     |                              |               |
    ///
    fn execute_rti(&mut self) {
        self.s += 1;
        self.p = self.read(0x0100 + self.s as u16);
        self.set_flag(StatusFlag::B, true);
        self.set_flag(StatusFlag::U, true);

        self.s += 1;
        let lo = self.read(0x0100 + self.s as u16) as u16;
        self.s += 1;
        let hi = self.read(0x0100 + self.s as u16) as u16;
        self.pc = (hi << 8) | lo;
    }

    /// Return from Subroutine.
    ///
    /// |Mnemonic|Operation                     |Condition Codes|
    /// |--------|------------------------------|---------------|
    /// |RTS     |                              |               |
    ///
    fn execute_rts(&mut self) {
        self.s += 1;
        let lo = self.read(0x0100 + self.s as u16) as u16;
        self.s += 1;
        let hi = self.read(0x0100 + self.s as u16) as u16;
        self.pc = (hi << 8) | lo;
        self.pc += 1;
    }

    /// Substract Memory from Accumulator with Borrow.
    ///
    /// |Mnemonic|Operation                     |Condition Codes|
    /// |--------|------------------------------|---------------|
    /// |SBC     |A = A - M - (1 - C)           |N, Z, C, V     |
    ///
    fn execute_sbc(&mut self) {
        self.fetch();

        let value = (self.fetched as u16) ^ 0x00FF;

        let temp = self.a as u16 + value + self.get_flag(StatusFlag::C) as u16;
        self.set_flag(StatusFlag::C, temp > 255);
        self.set_flag(StatusFlag::Z, (temp & 0x00FF) == 0);
        self.set_flag(
            StatusFlag::V,
            (temp ^ self.a as u16) & (temp ^ value) & 0x0080 > 0,
        );
        self.set_flag(StatusFlag::N, temp & 0x80 > 0);
        self.a = (temp & 0x00FF) as u8;
    }

    /// Set Carry Flag.
    ///
    /// |Mnemonic|Operation                     |Condition Codes|
    /// |--------|------------------------------|---------------|
    /// |SEC     |C = 1                         |C              |
    ///
    fn execute_sec(&mut self) {
        self.set_flag(StatusFlag::C, true);
    }

    /// Set Decimal Mode.
    ///
    /// |Mnemonic|Operation                     |Condition Codes|
    /// |--------|------------------------------|---------------|
    /// |SED     |D = 1                         |D              |
    ///
    fn execute_sed(&mut self) {
        self.set_flag(StatusFlag::D, true);
    }

    /// Set Interrupt Disable Status.
    ///
    /// |Mnemonic|Operation                     |Condition Codes|
    /// |--------|------------------------------|---------------|
    /// |SEI     |I = 1                         |I              |
    ///
    fn execute_sei(&mut self) {
        self.set_flag(StatusFlag::I, true);
    }

    /// Store Accumulator in Memory.
    ///
    /// |Mnemonic|Operation                     |Condition Codes|
    /// |--------|------------------------------|---------------|
    /// |STA     |M = A                         |               |
    ///
    fn execute_sta(&mut self) {
        self.write(self.addr_abs, self.a)
    }

    /// Store Index X in Memory.
    ///
    /// |Mnemonic|Operation                     |Condition Codes|
    /// |--------|------------------------------|---------------|
    /// |STX     |M = X                         |               |
    ///
    fn execute_stx(&mut self) {
        self.write(self.addr_abs, self.x)
    }

    /// Store Index Y in Memory.
    ///
    /// |Mnemonic|Operation                     |Condition Codes|
    /// |--------|------------------------------|---------------|
    /// |STY     |M = Y                         |               |
    ///
    fn execute_sty(&mut self) {
        self.write(self.addr_abs, self.y)
    }

    /// Transfer Accumulator to Index X.
    ///
    /// |Mnemonic|Operation                     |Condition Codes|
    /// |--------|------------------------------|---------------|
    /// |TAX     |X = A                         |N, Z           |
    ///
    fn execute_tax(&mut self) {
        self.x = self.a;
        self.set_flag(StatusFlag::Z, self.x == 0);
        self.set_flag(StatusFlag::N, self.x & 0x80 > 0);
    }

    /// Transfer Accumulator to Index Y.
    ///
    /// |Mnemonic|Operation                     |Condition Codes|
    /// |--------|------------------------------|---------------|
    /// |TAY     |Y = A                         |N, Z           |
    ///
    fn execute_tay(&mut self) {
        self.y = self.a;
        self.set_flag(StatusFlag::Z, self.y == 0);
        self.set_flag(StatusFlag::N, self.y & 0x80 > 0);
    }

    /// Transfer Stack Pointer to Index X.
    ///
    /// |Mnemonic|Operation                     |Condition Codes|
    /// |--------|------------------------------|---------------|
    /// |TSX     |X = SP                        |N, Z           |
    ///
    fn execute_tsx(&mut self) {
        self.x = self.s;
        self.set_flag(StatusFlag::Z, self.x == 0);
        self.set_flag(StatusFlag::N, self.x & 0x80 > 0);
    }

    /// Transfer Index X to Accumulator.
    ///
    /// |Mnemonic|Operation                     |Condition Codes|
    /// |--------|------------------------------|---------------|
    /// |TXA     |A = X                         |N, Z           |
    ///
    fn execute_txa(&mut self) {
        self.a = self.x;
        self.set_flag(StatusFlag::Z, self.a == 0);
        self.set_flag(StatusFlag::N, self.a & 0x80 > 0);
    }

    /// Transfer Index X to Stack Pointer.
    ///
    /// |Mnemonic|Operation                     |Condition Codes|
    /// |--------|------------------------------|---------------|
    /// |TXS     |SP = X                        |               |
    ///
    fn execute_txs(&mut self) {
        self.s = self.x;
    }

    /// Transfer Index Y to Accumulator.
    ///
    /// |Mnemonic|Operation                     |Condition Codes|
    /// |--------|------------------------------|---------------|
    /// |TYA     |A = Y                         |N, Z           |
    ///
    fn execute_tya(&mut self) {
        self.a = self.y;
        self.set_flag(StatusFlag::Z, self.a == 0);
        self.set_flag(StatusFlag::N, self.a & 0x80 > 0);
    }

    /// Illegal Opcode handling.
    fn execute_xxx(&mut self) {
        panic!(
            "demoNESse burst into flames because of unimplemented Opcode 0x{:02X?}",
            self.opcode
        );
    }

    /// Check if more cycles should be performed before executing the next instruction.
    pub fn complete(&self) -> bool {
        self.cycles == 0
    }

    //=============================================================================================
    // Interrupts
    //
    //=============================================================================================

    /// Shared interrupt mecanism.
    fn interrupt(&mut self, vector_addr: u16, software_interrupt: bool) {
        if vector_addr == RES_VECTOR {
            // A reset does not modify stack memory
            self.s -= 3;
        } else {
            // Store PC to the stack
            let pc_hi = ((self.pc >> 8) & 0x00FF) as u8;
            let pc_lo = (self.pc & 0x00FF) as u8;
            self.write(0x0100 + self.s as u16, pc_hi);
            self.s -= 1;
            self.write(0x0100 + self.s as u16, pc_lo);
            self.s -= 1;

            // The B flag
            // Nesdev Wiki: <https://www.nesdev.org/wiki/Status_flags#The_B_flag>
            if software_interrupt {
                self.set_flag(StatusFlag::B, true);
            } else {
                self.set_flag(StatusFlag::B, false);
            }
            self.set_flag(StatusFlag::U, true);

            // Store P to the stack
            self.write(0x0100 + self.s as u16, self.p);
            self.s -= 1;

            if software_interrupt {
                self.set_flag(StatusFlag::B, false);
            }
        }

        // Disable interrupts
        self.set_flag(StatusFlag::I, true);

        // Follow vector
        let lo = self.read(vector_addr) as u16;
        let hi = self.read(vector_addr + 1) as u16;
        self.pc = (hi << 8) | lo;
    }

    /// Interrupt Request.
    ///
    /// # Nesdev Wiki
    ///
    /// <https://www.nesdev.org/wiki/CPU_interrupts>
    ///
    /// # MOS Technology 6502 Datasheet
    ///
    /// > This TTL level input requests that an interrupt sequence begin within the microprocessor.
    /// The microprocessor will complete the current instruction being executed before recognizing
    /// the request. At that time, the interrupt mask bit in the Status Code Register will be
    /// examined. If the interrupt mask flag is not set, the microprocessor will begin an interrupt
    /// sequence.
    ///
    /// > The Program Counter and Processor Status Register are stored in the stack. The
    /// microprocessor will then set the interrupt mask flag high so that no further interrupt may
    /// occur. At the end of this cycle, the program counter low will be loaded from address FFFE,
    /// and counter high from location FFFF, therefore transferring program control to the memory
    /// vector located at these addresses.
    ///
    /// > The RDY signal must be in the high state for any interrupt to be recognized. A 3KΩ
    /// external resistor should be used for proper wire-OR operation.
    fn receive_irq(&mut self) {
        // Check if interrupts are disabled
        if self.get_flag(StatusFlag::I) == false {
            self.interrupt(IRQ_VECTOR, false);
            self.cycles = 7;
        }
    }

    /// Non-Maskable Interrupt.
    ///
    /// # Nesdev Wiki
    ///
    /// <https://www.nesdev.org/wiki/CPU_interrupts>
    ///
    /// # MOS Technology 6502 Datasheet
    ///
    /// > A negative going edge on this input requests that a non-maskable interrupt sequence be
    /// generated within the microprocessor.
    ///
    /// > NMI is an unconditional interrupt. Following completion of the current instruction, the
    /// sequence of operations is defined for IRQ will be performed, regardless of the state
    /// interrupt mask flag. The vector address loaded into the program counter, low and high, are
    /// locations FFFA and FFFB respectively, thereby transferring program control to the memory
    /// vector located at these addresses. The instruction loaded at these locations cause the
    /// microprocessor to branch to a non-maskable interrupt routine in memory.
    ///
    /// > NMI also requires an external 3KΩ register to Vcc for proper wire-OR operations.
    ///
    /// > Inputs IRQ and NMI are hardware interrupts lines that are sampled during φ2 and will
    /// begin the appropriate interrupt routine on the φ1 following the completion of the current
    /// instruction.
    fn receive_nmi(&mut self) {
        self.interrupt(NMI_VECTOR, false);
        self.cycles = 8;
    }

    /// Reset the microprocessor.
    ///
    /// # Nesdev Wiki
    ///
    /// <https://www.nesdev.org/wiki/CPU_power_up_state#After_reset>
    ///
    /// # MOS Technology 6502 Datasheet
    ///
    /// > This input is used to reset or start the microprocessor from a power down condition.
    /// During the time that this line is held low, writing to or from the microprocessor is
    /// inhibited. When a positive edge is detected on the input, the microprocessor will
    /// immediately begin the reset sequence.
    ///
    /// > After a system initialization time of six clock cycles, the mask interrupt flag will be
    /// set and the microprocessor will load the program counter from the memory vector locations
    /// FFFC and FFFD. This is the start location for program control.
    ///
    /// > After Vcc reaches 4.75 volts in a power up routine, reset must be held low for at least
    /// two clock cycles. At this time the R/W and (SYNC) signal will become valid.
    ///
    /// > When the reset signal goes high following these two clock cycles, the microprocessor
    /// will proceed with the normal reset procedure detailed above.
    pub fn reset(&mut self) {
        // As other interrupts, Reset write PC and P to the stack.
        // 27c3: Reverse Engineering the MOS 6502 CPU:
        //  <https://www.youtube.com/watch?v=fWqBmmPQP40&t=2535s>
        self.interrupt(RES_VECTOR, false);
        self.cycles = 6 + 2;
    }

    //=============================================================================================
    // Disassembly helper
    //=============================================================================================

    /// Disassemble code from memory to a human readable version.
    pub fn disassemble(&self, start: u16, end: u16, count: u16) -> BTreeMap<u16, String> {
        let mut map = BTreeMap::new();
        let mut address = start;
        let mut read = 0;

        while (end > 0 && address < end) || (count > 0 && read < count) {
            let base_address = address;

            let opcode = self.read(address);
            address += 1;
            let mut operand1 = None;
            let mut operand2 = None;

            let instruction = instructions::get(opcode);

            let (address_as_txt, mode_as_txt) = match instruction.am() {
                AddressingMode::ACC => ("".to_string(), "{ACC}"),
                AddressingMode::IMM => {
                    let value = self.read(address);
                    operand1 = Some(value);
                    address += 1;

                    (format!("#${:02X?}", value), "{IMM}")
                }
                AddressingMode::ABS => {
                    let lo = self.read(address);
                    operand1 = Some(lo);
                    address += 1;
                    let hi = self.read(address);
                    operand2 = Some(hi);
                    address += 1;

                    (format!("${:04X?}", (hi as u16) << 8 | lo as u16), "{IMM}")
                }
                AddressingMode::ZP0 => {
                    let lo = self.read(address);
                    operand1 = Some(lo);
                    address += 1;

                    (format!("${:02X?}", lo), "{ZP0}")
                }
                AddressingMode::ZPX => {
                    let lo = self.read(address);
                    operand1 = Some(lo);
                    address += 1;

                    (format!("${:02X?}", lo), "{ZPX}")
                }
                AddressingMode::ZPY => {
                    let lo = self.read(address);
                    operand1 = Some(lo);
                    address += 1;

                    (format!("${:02X?}", lo), "{ZPY}")
                }
                AddressingMode::ABX => {
                    let lo = self.read(address);
                    operand1 = Some(lo);
                    address += 1;
                    let hi = self.read(address);
                    operand2 = Some(hi);
                    address += 1;

                    (
                        format!("${:04X?}, X", (hi as u16) << 8 | lo as u16),
                        "{ABX}",
                    )
                }
                AddressingMode::ABY => {
                    let lo = self.read(address);
                    operand1 = Some(lo);
                    address += 1;
                    let hi = self.read(address);
                    operand2 = Some(hi);
                    address += 1;

                    (
                        format!("${:04X?}, Y", (hi as u16) << 8 | lo as u16),
                        "{ABY}",
                    )
                }
                AddressingMode::IMP => ("".to_string(), "{IMP}"),
                AddressingMode::REL => {
                    let value = self.read(address);
                    operand1 = Some(value);
                    address += 1;
                    let dest = address.wrapping_add_signed(value as i8 as i16);
                    let dest_lo = (dest & 0x00FF) as u8;
                    let dest_hi = ((dest & 0xFF00) >> 8) as u8;

                    (
                        format!(
                            "${:02X?} [${:04X?}]",
                            value,
                            (dest_hi as u16) << 8 | dest_lo as u16
                        ),
                        "{REL}",
                    )
                }
                AddressingMode::IZX => {
                    let lo = self.read(address);
                    operand1 = Some(lo);
                    address += 1;

                    (format!("(${:02X?}, X)", lo), "{IZX}")
                }
                AddressingMode::IZY => {
                    let lo = self.read(address);
                    operand1 = Some(lo);
                    address += 1;

                    (format!("(${:02X?}, Y)", lo), "{IZY}")
                }
                AddressingMode::IND => {
                    let lo = self.read(address);
                    operand1 = Some(lo);
                    address += 1;
                    let hi = self.read(address);
                    operand2 = Some(hi);
                    address += 1;

                    (
                        format!("(${:04X?}, Y)", (hi as u16) << 8 | lo as u16),
                        "{IND}",
                    )
                }
            };

            let as_txt = format!(
                "{} {:<11} {}",
                instruction.op(),
                address_as_txt,
                mode_as_txt
            );
            let as_bytes = match (operand1, operand2) {
                (Some(op1), Some(op2)) => format!("{:02X?} {:02X?} {:02X?}", opcode, op1, op2),
                (Some(op1), None) => format!("{:02X?} {:02X?}", opcode, op1),
                _ => format!("{:02X?}", opcode),
            };
            map.insert(
                base_address,
                format!("${:04X?} {:<9} {}", base_address, as_bytes, as_txt),
            );
            read += 1;
        }
        map
    }
}
