use super::Mapper;

pub struct Mapper000 {
    prg_banks: usize,
    chr_banks: usize,
}

impl Mapper000 {
    pub fn new(prg_banks: usize, chr_banks: usize) -> Self {
        Self {
            prg_banks,
            chr_banks,
        }
    }
}

impl Mapper for Mapper000 {
    fn cpu_read(&self, addr: u16, mapped_addr: &mut u16) -> bool {
        if addr >= 0x8000 {
            if self.prg_banks > 1 {
                *mapped_addr = addr & 0x7FFF;
            } else {
                *mapped_addr = addr & 0x3FFF;
            }
            true
        } else {
            false
        }
    }

    fn cpu_write(&self, _addr: u16, _mapped_addr: &mut u16) -> bool {
        // We don't write to PRG ROM
        false
    }

    fn ppu_read(&self, addr: u16, mapped_addr: &mut u16) -> bool {
        if addr <= 0x1FFF {
            *mapped_addr = addr;
            true
        } else {
            false
        }
    }

    fn ppu_write(&self, _addr: u16, _mapped_addr: &mut u16) -> bool {
        // We don't write to CHR ROM
        false
    }
}
