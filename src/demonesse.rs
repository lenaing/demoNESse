use std::sync::{Arc, RwLock};

mod bus;
pub mod cartridge;
mod cpu;
pub mod debugger;
pub mod frontend;
pub mod gui;
mod mapper;
mod ppu;

use self::{bus::Bus, cartridge::Cartridge, cpu::CPU, ppu::PPU};

/// DemoNESse core.
#[derive(Clone)]
pub struct DemoNESse {
    /// Main Bus.
    bus: Arc<RwLock<Bus>>,
    /// NES CPU.
    cpu: Arc<RwLock<CPU>>,
    /// NES PPU.
    ppu: Arc<RwLock<PPU>>,
    /// How many clocks have passed.
    clock_counter: u32,
    /// Is DemoNESse running?
    running: bool,
    /// Is DemoNESse debugging?
    debugging: bool,
}

impl DemoNESse {
    pub fn new() -> Self {
        let ppu = Arc::new(RwLock::new(PPU::new()));
        // Wire PPU to Bus
        let bus = Arc::new(RwLock::new(Bus::new(Arc::clone(&ppu))));
        // Wire CPU to Bus
        let cpu = Arc::new(RwLock::new(CPU::new(Arc::clone(&bus))));

        Self {
            bus,
            cpu,
            ppu,
            clock_counter: 0,
            running: true,
            debugging: false,
        }
    }

    pub fn is_running(&self) -> bool {
        self.running
    }

    pub fn is_debugging(&self) -> bool {
        self.debugging
    }

    pub fn reset(&mut self) {
        self.cpu.write().unwrap().reset();
        self.clock_counter = 0;
    }

    fn clock(&mut self) -> (bool, bool) {
        let cpu_clock = self.clock_counter % 3 == 0;
        let frame_complete = self.ppu.write().unwrap().clock();
        if cpu_clock {
            self.cpu.write().unwrap().clock();
        }
        self.clock_counter += 1;
        (cpu_clock, frame_complete)
    }

    pub fn insert_cartridge(&mut self, cartridge: Arc<RwLock<Cartridge>>) {
        self.bus.write().unwrap().insert_cartridge(cartridge);
    }
}
