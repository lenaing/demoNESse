#![allow(non_snake_case)] // https://github.com/rust-lang/rust/issues/45127
#![feature(mixed_integer_ops)]
mod demonesse;

use std::{
    sync::{Arc, RwLock},
    thread,
};

use demonesse::{cartridge::Cartridge, frontend::SDL2Frontend, gui::GUI, DemoNESse};

use crate::demonesse::debugger::cli::CLIDebugger;

fn main() {
    // Load cartridge
    let cartridge = Cartridge::new("ressources/nestest.nes");

    // Create system, load Cartridge and reset system
    let mut demonesse = DemoNESse::new();
    demonesse.insert_cartridge(Arc::new(RwLock::new(cartridge)));
    demonesse.reset();

    let demonesse = Arc::new(RwLock::new(demonesse));

    // Start CLI Debugger
    let demonesse_clone = Arc::clone(&demonesse);
    thread::spawn(move || {
        CLIDebugger::new(demonesse_clone).shell();
    });

    // Start GUI with an SDL2 Frontend
    GUI::new(demonesse, 0, Box::new(SDL2Frontend::new(false))).run();
}
